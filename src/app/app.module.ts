import {NgModule, ErrorHandler} from '@angular/core';
import {IonicApp, IonicModule, IonicErrorHandler, IonicPageModule} from 'ionic-angular';
import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {ResultsPage} from "../pages/results/results";
import {HelloIonicPage} from "../pages/hello-ionic/hello-ionic";
import {ProfileServicesPage} from "../pages/profile/profile-services/profile-services";
import {ProfilePage} from "../pages/profile/profile";
import {SearchPage} from "../pages/search/search";
import {DataPipe} from "../modules/Data.pipe";
import {ProfileMapPage} from "../pages/profile/profile-map/profile-map";
import {ProfileImagesPage} from "../pages/profile/profile-images/profile-images";
import {AppointmentPage} from "../pages/appointment/appointment";
import {AppointmentSchedulePage} from "../pages/appointment/appointment-schedule/appointment-schedule";
import {LoginPage} from "../pages/login/login";
import {CallingPage} from "../pages/calling/calling";
import {SettingsPage} from "../pages/settings/settings";
import {TermsPage} from "../pages/terms/terms";
import {AboutUsPage} from "../pages/about-us/about-us";
import {AppointmentsPage} from "../pages/appointments/appointments";
import {AccountPage} from "../pages/account/account";
import {NotificationsPage} from "../pages/notifications/notifications";
import {AppointmentPersonPage} from "../pages/appointment/appointment-person/appointment-person";
import {AppointmentServicePage} from "../pages/appointment/appointment-service/appointment-service";
import {Rating} from "../modules/Rating";
import {AppointmentHospitalPage} from "../pages/appointment/appointment-hospital/appointment-hospital";
import {AppointmentsDetailsPage} from "../pages/appointments/appointments-details/appointments-details";
import {ProfileServicesShowPage} from "../pages/profile/profile-services/profile-services-show/profile-services-show";
import {ResultsFilterPage} from "../pages/results/results-filter/results-filter";
import {SpecialitiesPage} from "../pages/specialities/specialities";
import {SignupPage} from "../pages/signup/signup";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {SplashScreen} from "@ionic-native/splash-screen";
import {StatusBar} from "@ionic-native/status-bar";
import { InViewportModule } from 'ng-in-viewport';
import {Deeplinks} from "@ionic-native/deeplinks";
import {Facebook} from "@ionic-native/facebook";


@NgModule({
  declarations: [
    MyApp,
    HelloIonicPage,
    Rating,
    ProfileMapPage,
    ResultsPage,
    ProfileServicesPage,
    ProfileServicesShowPage,
    ProfilePage,
    ResultsFilterPage,
    SignupPage,
    HomePage,
    SearchPage,
    DataPipe,
    AppointmentPage,
    AppointmentSchedulePage,
    ProfileImagesPage,
    LoginPage,
    CallingPage,
    SettingsPage,
    TermsPage,
    AboutUsPage,
    AppointmentsPage,
    AppointmentsDetailsPage,
    AppointmentPersonPage,
    AppointmentServicePage,
    AppointmentHospitalPage,
    SpecialitiesPage,
    AccountPage,
    NotificationsPage,
  ],
  imports: [
    BrowserModule,  // New in ionic 3
    HttpModule,  // New in ionic 3
    InViewportModule.forRoot(),
    IonicModule.forRoot(MyApp, {
      config: {
        // These options are available in ionic-angular@2.0.0-beta.2 and up.
        scrollAssist: false,    // Valid options appear to be [true, false]
        autoFocusAssist: false  // Valid options appear to be ['instant', 'delay', false]
      },
      platforms: {
        android: {
          statusbarPadding: true
        }
      }
    })

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HelloIonicPage,
    ProfileMapPage,
    ResultsPage,
    ProfileServicesPage,
    ProfileServicesShowPage,
    ProfilePage,
    ResultsFilterPage,
    HomePage,
    SearchPage,
    AppointmentPage,
    ProfileImagesPage,
    AppointmentSchedulePage,
    LoginPage,
    CallingPage,
    SettingsPage,
    TermsPage,
    AboutUsPage,
    AppointmentsPage,
    SignupPage,
    AppointmentsDetailsPage,
    AppointmentPersonPage,
    AppointmentServicePage,
    AppointmentHospitalPage,
    SpecialitiesPage,

    AccountPage,
    NotificationsPage,
  ],
  providers: [
    Deeplinks,
    SplashScreen,
    Facebook,
    StatusBar,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {
}

