"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ionic_native_1 = require("ionic-native");
var home_1 = require("../pages/home/home");
var Global_1 = require("../modules/Global");
var Auth_1 = require("../modules/Auth");
var settings_1 = require("../pages/settings/settings");
var calling_1 = require("../pages/calling/calling");
var terms_1 = require("../pages/terms/terms");
var about_us_1 = require("../pages/about-us/about-us");
var appointments_1 = require("../pages/appointments/appointments");
var signup_1 = require("../pages/signup/signup");
var appointments_details_1 = require("../pages/appointments/appointments-details/appointments-details");
var Appointment_1 = require("../eloquent/Appointment");
var MyApp = (function () {
    function MyApp(platform, http, toastCtrl, deeplinks, statusBar, splashScreen, ionicApp, menuCtrl, menu, modalCtrl) {
        var _this = this;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.deeplinks = deeplinks;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.ionicApp = ionicApp;
        this.menuCtrl = menuCtrl;
        this.menu = menu;
        this.modalCtrl = modalCtrl;
        this.rootPage = home_1.HomePage;
        this.isRootPage = true;
        this.user = Auth_1.Auth.user();
        this.pages = {
            settings: settings_1.SettingsPage,
            terms: terms_1.TermsPage,
            about_us: 'AboutUsPage',
            appointments: appointments_1.AppointmentsPage
        };
        new Global_1.Global(http, toastCtrl, modalCtrl);
        if (this.user) {
            Auth_1.Auth.getUser().then(function (a) { return _this.user = a; });
        }
        this.deeplinks.route({
            '/about-us': about_us_1.AboutUsPage,
        }).subscribe(function (match) {
            // match.$route - the route we matched, which is the matched entry from the arguments to route()
            // match.$args - the args passed in the link
            // match.$link - the full link data
            console.log('Successfully matched route', match);
        }, function (nomatch) {
            // nomatch.$link - the full link data
            console.error('Got a deeplink that didn\'t match', nomatch);
        });
        platform.ready().then(function () {
            //this.nav.push(CallingPage);
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleBlackTranslucent();
            // StatusBar.backgroundColorByHexString("#2d75f1");
            //      keyboard.disableScroll(true);
            splashScreen.hide();
            var ready = true;
            platform.registerBackButtonAction(function () {
                var activePortal = ionicApp._loadingPortal.getActive() ||
                    ionicApp._modalPortal.getActive() ||
                    ionicApp._toastPortal.getActive() ||
                    ionicApp._overlayPortal.getActive();
                if (activePortal) {
                    ready = false;
                    activePortal.dismiss().catch(function () {
                    });
                    activePortal.onDidDismiss(function () {
                        ready = true;
                    });
                    return;
                }
                if (menuCtrl.isOpen()) {
                    menuCtrl.close().catch(function () {
                    });
                    return;
                }
                var view = _this.nav.getActive();
                var page = view ? _this.nav.getActive().instance : null;
                console.log(page, view);
                if (page && page.isRootPage) {
                    window['plugins'].appMinimize.minimize();
                    return;
                }
                else if (_this.nav.canGoBack() || view && view.isOverlay) {
                    _this.nav.pop().catch(function () {
                    });
                    return;
                }
                else if (!_this.nav.canGoBack() && view && view.isOverlay) {
                    _this.nav.pop().catch(function () {
                    });
                    return;
                }
            }, 1);
            if (!platform.is('browser')) {
                var push = ionic_native_1.Push.init({
                    android: {
                        senderID: "882582227414",
                        icon: "www/assets/img/logo-icon-50.png"
                    },
                    ios: {
                        alert: "true",
                        badge: false,
                        sound: "true"
                    },
                    windows: {}
                });
                if (typeof push.on === 'function') {
                    push.on('registration', function (data) {
                        Auth_1.Auth.SetToken(data.registrationId);
                    });
                    push.on('notification', function (data) {
                        console.log('message', data);
                        if (data.additionalData.response) {
                            _this.nav.push(calling_1.CallingPage, data.additionalData).catch(function () {
                            });
                        }
                        var active = _this.nav.getActive().component;
                        var toast = _this.toastCtrl.create({
                            message: data.message,
                            duration: 4000,
                            position: 'bottom'
                        });
                        toast.present().catch(null);
                        if (data.additionalData.collapse_key == 'programare-confirmata') {
                            if (active === appointments_details_1.AppointmentsDetailsPage)
                                _this.nav.pop();
                            new Appointment_1.Appointment().find(data.additionalData.app).later(function (a) {
                                _this.nav.push(appointments_details_1.AppointmentsDetailsPage, a).catch(function () {
                                });
                            });
                        }
                    });
                }
                window['answer'] = function (data) {
                    console.log('action.answer', data);
                    _this.nav.push(calling_1.CallingPage, data.additionalData);
                };
            }
            if (platform.is('android'))
                _this.permissions();
            //
            // let now = new Date().getTime(),
            //   _5_sec_from_now = new Date(now + 15 * 1000);
            //
            //
            // LocalNotifications.schedule({
            //   id: 1,
            //   title: 'Scheduled with delay',
            //
            //   text: 'Single ILocalNotification',
            //   at: _5_sec_from_now,
            //   badge: 12
            //
            // });
            //
            //
            // LocalNotifications.schedule({
            //   id: 1,
            //   title: 'First one to test',
            //   text: 'Single ILocalNotification',
            //   badge: 12
            //
            // });
        });
    }
    MyApp.prototype.openPage = function (page) {
        // let profileModal = this.modalCtrl.create(page);
        // profileModal.present();
        this.menu.close();
        this.nav.push(page);
    };
    MyApp.prototype.permissions = function () {
        if (!window['cordova'])
            return;
        var permissions = window['cordova'].plugins.permissions;
        if (!permissions)
            return;
        permissions.hasPermission(permissions.CAMERA, checkPermissionCallback, null);
        permissions.hasPermission(permissions.RECORD_AUDIO, function (status) {
            if (!status.hasPermission)
                permissions.requestPermission(permissions.RECORD_AUDIO);
        }, null);
        function checkPermissionCallback(status) {
            if (!status.hasPermission) {
                var errorCallback_1 = function () {
                    console.warn('Camera permission is not turned on');
                };
                permissions.requestPermission(permissions.CAMERA, function (status) {
                    if (!status.hasPermission)
                        errorCallback_1();
                }, errorCallback_1);
            }
        }
    };
    MyApp.prototype.login = function () {
        var _this = this;
        var profileModal = this.modalCtrl.create('LoginPage');
        profileModal.onDidDismiss(function (data) {
            Auth_1.Auth.getUser().then(function (a) { return _this.user = a; });
        });
        profileModal.present().then(function () {
        }).catch(function (e) { return e.toString(); });
    };
    MyApp.prototype.logout = function () {
        Auth_1.Auth.setUser(null);
        this.user = null;
    };
    MyApp.prototype.signup = function () {
        var _this = this;
        var profileModal = this.modalCtrl.create(signup_1.SignupPage);
        profileModal.onDidDismiss(function (data) {
            Auth_1.Auth.getUser().then(function (a) { return _this.user = a; });
        });
        profileModal.present().then(function () {
        }).catch(function (e) { return e.toString(); });
    };
    return MyApp;
}());
__decorate([
    core_1.ViewChild('content')
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    core_1.Component({
        templateUrl: 'app.html',
    })
], MyApp);
exports.MyApp = MyApp;
