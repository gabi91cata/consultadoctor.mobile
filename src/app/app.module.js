"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var app_component_1 = require("./app.component");
var home_1 = require("../pages/home/home");
var results_1 = require("../pages/results/results");
var hello_ionic_1 = require("../pages/hello-ionic/hello-ionic");
var profile_services_1 = require("../pages/profile/profile-services/profile-services");
var profile_1 = require("../pages/profile/profile");
var search_1 = require("../pages/search/search");
var Data_pipe_1 = require("../modules/Data.pipe");
var profile_map_1 = require("../pages/profile/profile-map/profile-map");
var profile_images_1 = require("../pages/profile/profile-images/profile-images");
var appointment_1 = require("../pages/appointment/appointment");
var appointment_schedule_1 = require("../pages/appointment/appointment-schedule/appointment-schedule");
var login_1 = require("../pages/login/login");
var calling_1 = require("../pages/calling/calling");
var settings_1 = require("../pages/settings/settings");
var terms_1 = require("../pages/terms/terms");
var about_us_1 = require("../pages/about-us/about-us");
var appointments_1 = require("../pages/appointments/appointments");
var account_1 = require("../pages/account/account");
var notifications_1 = require("../pages/notifications/notifications");
var appointment_person_1 = require("../pages/appointment/appointment-person/appointment-person");
var appointment_service_1 = require("../pages/appointment/appointment-service/appointment-service");
var Rating_1 = require("../modules/Rating");
var appointment_hospital_1 = require("../pages/appointment/appointment-hospital/appointment-hospital");
var appointments_details_1 = require("../pages/appointments/appointments-details/appointments-details");
var profile_services_show_1 = require("../pages/profile/profile-services/profile-services-show/profile-services-show");
var results_filter_1 = require("../pages/results/results-filter/results-filter");
var specialities_1 = require("../pages/specialities/specialities");
var signup_1 = require("../pages/signup/signup");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var splash_screen_1 = require("@ionic-native/splash-screen");
var status_bar_1 = require("@ionic-native/status-bar");
var ng_in_viewport_1 = require("ng-in-viewport");
var deeplinks_1 = require("@ionic-native/deeplinks");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        declarations: [
            app_component_1.MyApp,
            hello_ionic_1.HelloIonicPage,
            Rating_1.Rating,
            profile_map_1.ProfileMapPage,
            results_1.ResultsPage,
            profile_services_1.ProfileServicesPage,
            profile_services_show_1.ProfileServicesShowPage,
            profile_1.ProfilePage,
            results_filter_1.ResultsFilterPage,
            signup_1.SignupPage,
            home_1.HomePage,
            search_1.SearchPage,
            Data_pipe_1.DataPipe,
            appointment_1.AppointmentPage,
            appointment_schedule_1.AppointmentSchedulePage,
            profile_images_1.ProfileImagesPage,
            login_1.LoginPage,
            calling_1.CallingPage,
            settings_1.SettingsPage,
            terms_1.TermsPage,
            about_us_1.AboutUsPage,
            appointments_1.AppointmentsPage,
            appointments_details_1.AppointmentsDetailsPage,
            appointment_person_1.AppointmentPersonPage,
            appointment_service_1.AppointmentServicePage,
            appointment_hospital_1.AppointmentHospitalPage,
            specialities_1.SpecialitiesPage,
            account_1.AccountPage,
            notifications_1.NotificationsPage,
        ],
        imports: [
            platform_browser_1.BrowserModule,
            http_1.HttpModule,
            ng_in_viewport_1.InViewportModule.forRoot(),
            ionic_angular_1.IonicModule.forRoot(app_component_1.MyApp, {
                config: {
                    // These options are available in ionic-angular@2.0.0-beta.2 and up.
                    scrollAssist: false,
                    autoFocusAssist: false // Valid options appear to be ['instant', 'delay', false]
                },
                platforms: {
                    android: {
                        statusbarPadding: true
                    }
                }
            })
        ],
        bootstrap: [ionic_angular_1.IonicApp],
        entryComponents: [
            app_component_1.MyApp,
            hello_ionic_1.HelloIonicPage,
            profile_map_1.ProfileMapPage,
            results_1.ResultsPage,
            profile_services_1.ProfileServicesPage,
            profile_services_show_1.ProfileServicesShowPage,
            profile_1.ProfilePage,
            results_filter_1.ResultsFilterPage,
            home_1.HomePage,
            search_1.SearchPage,
            appointment_1.AppointmentPage,
            profile_images_1.ProfileImagesPage,
            appointment_schedule_1.AppointmentSchedulePage,
            login_1.LoginPage,
            calling_1.CallingPage,
            settings_1.SettingsPage,
            terms_1.TermsPage,
            about_us_1.AboutUsPage,
            appointments_1.AppointmentsPage,
            signup_1.SignupPage,
            appointments_details_1.AppointmentsDetailsPage,
            appointment_person_1.AppointmentPersonPage,
            appointment_service_1.AppointmentServicePage,
            appointment_hospital_1.AppointmentHospitalPage,
            specialities_1.SpecialitiesPage,
            account_1.AccountPage,
            notifications_1.NotificationsPage,
        ],
        providers: [
            deeplinks_1.Deeplinks,
            splash_screen_1.SplashScreen,
            status_bar_1.StatusBar,
            { provide: core_1.ErrorHandler, useClass: ionic_angular_1.IonicErrorHandler }
        ]
    })
], AppModule);
exports.AppModule = AppModule;
