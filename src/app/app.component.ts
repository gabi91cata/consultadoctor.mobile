import {Component, ViewChild} from '@angular/core';
import {
  Platform, ToastController, ModalController, NavController, MenuController, ViewController,
  IonicApp
} from 'ionic-angular';
import {Push, Sim} from 'ionic-native';
import {HomePage} from "../pages/home/home";
import {Http} from "@angular/http";
import {Global} from "../modules/Global";
import {LoginPage} from "../pages/login/login";
import {Auth} from "../modules/Auth";
import {User} from "../eloquent/User";
import {SettingsPage} from "../pages/settings/settings";
import {CallingPage} from "../pages/calling/calling";
import {TermsPage} from "../pages/terms/terms";
import {AboutUsPage} from "../pages/about-us/about-us";
import {AppointmentsPage} from "../pages/appointments/appointments";
import {SignupPage} from "../pages/signup/signup";
import {StatusBar} from "@ionic-native/status-bar";
import {SplashScreen} from "@ionic-native/splash-screen";
import {AppointmentsDetailsPage} from "../pages/appointments/appointments-details/appointments-details";
import {Appointment} from "../eloquent/Appointment";
import {Deeplinks} from "@ionic-native/deeplinks";
import {Linking} from "../modules/Linking";


@Component({
  templateUrl: 'app.html',

})
export class MyApp {
  rootPage = HomePage;
  isRootPage = true;

  @ViewChild('content') nav: NavController;
  user: User = Auth.user();

  pages = {
    settings: SettingsPage,
    terms: TermsPage,
    about_us: AboutUsPage,
    appointments: AppointmentsPage
  };


  constructor(public platform: Platform, http: Http,private toastCtrl: ToastController, private deeplinks:Deeplinks, private statusBar: StatusBar, private splashScreen: SplashScreen, private ionicApp: IonicApp,
              private menuCtrl: MenuController, public menu: MenuController, public modalCtrl: ModalController) {
    new Global(http, toastCtrl, modalCtrl);

    if (this.user) {
      Auth.getUser().then((a: any) => this.user = a);

    }
    new Linking(deeplinks,this.nav);

    platform.ready().then(() => {

      Sim.getSimInfo().then(
        (info) => console.log('Sim info: ', info),
        (err) => console.log('Unable to get sim info: ', err)
      );



      statusBar.styleBlackTranslucent();
      splashScreen.hide();
      let ready = true;
      platform.registerBackButtonAction(() => {

        let activePortal = ionicApp._loadingPortal.getActive() ||
          ionicApp._modalPortal.getActive() ||
          ionicApp._toastPortal.getActive() ||
          ionicApp._overlayPortal.getActive();

        if (activePortal) {
          ready = false;
          activePortal.dismiss().catch(() => {
          });
          activePortal.onDidDismiss(() => {
            ready = true;
          });

          return;
        }

        if (menuCtrl.isOpen()) {
          menuCtrl.close().catch(() => {
          });

          return;
        }

        let view = this.nav.getActive();
        let page = view ? this.nav.getActive().instance : null;

        console.log(page, view);

        if (page && page.isRootPage) {

          window['plugins'].appMinimize.minimize();

          return;
        }
        else if (this.nav.canGoBack() || view && view.isOverlay
        ) {
          this.nav.pop().catch(() => {
          });
          return;
        } else if (!this.nav.canGoBack() && view && view.isOverlay
        ) {
          this.nav.pop().catch(() => {
          });
          return;
        }


      }, 1);


      if (!platform.is('browser')) {
        let push = Push.init({
          android: {
            senderID: "882582227414",
            icon: "www/assets/img/logo-icon-50.png"
          },
          ios: {
            alert: "true",
            badge: false,
            sound: "true"
          },
          windows: {}
        });
        if (typeof push.on === 'function') {
          push.on('registration', (data) => {
            Auth.SetToken(data.registrationId);
          });
          push.on('notification', (data: any) => {
            console.log('message', data);
            if (data.additionalData.response) {
              this.nav.push(CallingPage, data.additionalData).catch(() => {
              });
            }
            let active = this.nav.getActive().component;

            let toast = this.toastCtrl.create({
              message: data.message,
              duration:4000,
              position: 'bottom'
            });
            toast.present().catch(null);
            if (data.additionalData.collapse_key == 'programare-confirmata') {
              if (active === AppointmentsDetailsPage)
                this.nav.pop();

              new Appointment().find(data.additionalData.app).later((a) => {
                this.nav.push(AppointmentsDetailsPage, a).catch(() => {
                });

              });


            }
          });

        }


        window['answer'] = (data) => {
          console.log('action.answer', data);

          this.nav.push(CallingPage, data.additionalData);


        };


      }


      if (platform.is('android'))
        this.permissions();

      //
      // let now = new Date().getTime(),
      //   _5_sec_from_now = new Date(now + 15 * 1000);
      //
      //
      // LocalNotifications.schedule({
      //   id: 1,
      //   title: 'Scheduled with delay',
      //
      //   text: 'Single ILocalNotification',
      //   at: _5_sec_from_now,
      //   badge: 12
      //
      // });
      //
      //
      // LocalNotifications.schedule({
      //   id: 1,
      //   title: 'First one to test',
      //   text: 'Single ILocalNotification',
      //   badge: 12
      //
      // });


    })


  }

  openPage(page) {
    // let profileModal = this.modalCtrl.create(page);
    // profileModal.present();

    this.menu.close();

    this.nav.push(page);


  }


  permissions() {
    if (!window['cordova'])
      return;
    let permissions = window['cordova'].plugins.permissions;
    if (!permissions)
      return;
    permissions.hasPermission(permissions.CAMERA, checkPermissionCallback, null);
    permissions.hasPermission(permissions.RECORD_AUDIO, (status) => {
      if (!status.hasPermission)
        permissions.requestPermission(permissions.RECORD_AUDIO);
    }, null);


    function checkPermissionCallback(status) {
      if (!status.hasPermission) {
        let errorCallback = function () {
          console.warn('Camera permission is not turned on');
        };

        permissions.requestPermission(
          permissions.CAMERA,
          function (status) {
            if (!status.hasPermission) errorCallback();
          },
          errorCallback);
      }
    }
  }

  login() {

    let profileModal = this.modalCtrl.create(LoginPage);
    profileModal.onDidDismiss(data => {
      Auth.getUser().then((a: any) => this.user = a);
    });

    profileModal.present().then(() => {
    }).catch(e => e.toString());

  }

  logout(){
    Auth.setUser(null);
    this.user = null;
  }
  signup() {

    let profileModal = this.modalCtrl.create(SignupPage);
    profileModal.onDidDismiss(data => {
      Auth.getUser().then((a: any) => this.user = a);
    });

    profileModal.present().then(() => {
    }).catch(e => e.toString());

  }
}
