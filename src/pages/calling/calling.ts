import {Component} from '@angular/core';
import {NavController, NavParams, Platform} from 'ionic-angular';
import {Phone} from "../../modules/Phone";
import {User} from "../../eloquent/User";

/*
  Generated class for the Calling page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-calling',
  templateUrl: 'calling.html'
})
export class CallingPage {
  phone:Phone ;
  channel_id;
  otherUser:User;


  constructor(public navCtrl: NavController, platform:Platform, public navParams: NavParams) {

    this.channel_id = this.navParams.data.channel_id;
    this.otherUser = new User(this.navParams.data.user);

  }

  ionViewDidEnter() {



    this.phone =  new Phone(this.channel_id);
    this.phone.onEndCall = (type)=>{

      this.phone.stop();
      //this.phone = null;
      this.navCtrl.pop().catch(()=>{})

    };
    this.phone.onRegister = ()=>{
      this.phone.sendMessage('connected','');
      this.phone.autoAnswer(true);
    }

  }


  ionViewCanLeave(){
    return this.phone.type != 'accepted';
  }




}
