import { Component } from '@angular/core';
import {NavParams, ViewController, Platform} from 'ionic-angular';
import {Doctor} from "../../../eloquent/Doctor";
import {StatusBar} from "ionic-native";

/*
  Generated class for the ProfileImages page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile-images',
  templateUrl: 'profile-images.html'
})
export class ProfileImagesPage {

  doctor:Doctor;
  constructor(public viewCtrl: ViewController, public platform:Platform, public params:NavParams) {
    this.doctor = params.data.doctor;
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

     // StatusBar.hide();

      //GeoModule.getCoords();
      StatusBar.backgroundColorByHexString("#000000");


    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
    //StatusBar.show();
  }
  ionViewDidLoad() {
    console.log('Hello ProfileImagesPage Page');
  }

}
