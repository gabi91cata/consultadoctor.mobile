import {Component, ViewChild} from '@angular/core';
import {NavParams, NavController, ModalController, LoadingController, Content, Slides, Platform} from 'ionic-angular';
import {ProfileServicesPage} from "./profile-services/profile-services";
import {Http} from "@angular/http";
import {ProfileImagesPage} from "./profile-images/profile-images";
import {ProfileMapPage} from "./profile-map/profile-map";
import {Doctor} from "../../eloquent/Doctor";
import {AppointmentPage} from "../appointment/appointment";
import {Global} from "../../modules/Global";
import {HidingHeader} from "../../modules/HidingHeader";
import {AppointmentsDetailsPage} from "../appointments/appointments-details/appointments-details";

/*
 Generated class for the Profile page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage extends HidingHeader{
  @ViewChild("hours") sliderHour: Slides;
  @ViewChild("content") content: Content;

  loading;
  pages = {
    images: ProfileImagesPage,
    map: ProfileMapPage,
    services: ProfileServicesPage
  };
  doctor;
  free:any = [];
  options: any;
  width;
  tabType = 'appointment';

  constructor(public navCtrl: NavController, public platform:Platform, public params: NavParams, public http: Http, public modalCtrl: ModalController, public loadingCtrl: LoadingController) {
    super();
    this.doctor = params.data;
    this.width = window.outerWidth;
    if(!this.doctor.user_id)
    new Doctor().find(this.doctor.id).later((doctor) => {

      this.doctor = doctor;
    });

  }

  loadHours()
  {
    new Doctor().with({free:true}).find(this.doctor.id, false).later((response)=>{
      this.free = response;
    });
  }
  ionViewDidLoad()
  {
    this.loadHours();
  }

  ionViewDidEnter()
  {
    super.onScroll(this.content);
  }



  openServices(page, h) {

    let hospitals = this.doctor.hospitals.filter(item => item.id == h.hospital.id);

    this.navCtrl.push(page, {
      hospital:hospitals[0],
      h:h,
      profile:this
    });

  }

  openAppointment(hospital = null, firstFree = null, service = null) {

    Global.stopMessages();
    let profileModal = this.modalCtrl.create(AppointmentPage, {
      service: service?service:hospital.service,
      hospital: hospital.hospital.hospital,
      free:this.free,
      doctor: this.doctor,
      data: null,
      firstFree: firstFree
    });
    profileModal.onDidDismiss((r) => {
      if (r) {
        let ctrl = this.navCtrl.push(AppointmentsDetailsPage, r).then(null);

        new Doctor().find(this.doctor.id).later((a) => {
          this.doctor = a;
        });
      }
    });
    profileModal.present();
  }



  navigate(hospital)
  {
    let destination = hospital.lat + ',' + hospital.lng;

    if(this.platform.is('ios')){
      window.open('maps://?q=' + destination, '_system');
    } else {
      let label = encodeURI(hospital.name);
      window.open('geo:0,0?q=' + destination + '(' + label + ')', '_system');
    }
  }


}


