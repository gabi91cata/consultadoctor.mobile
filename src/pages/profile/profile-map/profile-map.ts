
import {Component} from '@angular/core';
import {NavController,  Platform} from 'ionic-angular';


/*
 Generated class for the ProfileMap page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
    selector: 'page-profile-map',
    templateUrl: 'profile-map.html'
})
export class ProfileMapPage {



    constructor(public navCtrl: NavController, public platform: Platform) {
        platform.ready().then(() => {
            this.loadMap();
        });
    }

    loadMap() {


    }

}
