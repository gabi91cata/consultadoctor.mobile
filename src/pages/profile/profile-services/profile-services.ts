import { Component } from '@angular/core';
import {NavController, NavParams, ModalController} from 'ionic-angular';
import {Http} from "@angular/http";
import {ProfileServicesShowPage} from "./profile-services-show/profile-services-show";
import {ProfilePage} from "../profile";

/*
  Generated class for the ProfileServices page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile-services',
  templateUrl: 'profile-services.html'
})
export class ProfileServicesPage {

  hospital;
  h;
  doctor;
  services;
  profile:ProfilePage;
  constructor(public navCtrl: NavController, public modalCtrl:ModalController, public http:Http, public params:NavParams) {
    let data = params.data;
    this.services = data.hospital.services;
    this.doctor = data.hospital.doctor;
    this.hospital = data.hospital.hospital;
    this.h = data.h;
    this.profile = data.profile;
  }

  open(service){
    this.navCtrl.push(ProfileServicesShowPage, {
      service:service,
      doctor:this.doctor,
      hospital:this.hospital,
      h:this.h,
      profile:this.profile
    }).then(null);
  }



}
