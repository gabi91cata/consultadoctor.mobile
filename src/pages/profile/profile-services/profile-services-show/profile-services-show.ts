import { Component } from '@angular/core';
import {NavController, NavParams, ModalController} from 'ionic-angular';
import {Http} from "@angular/http";
import {ProfilePage} from "../../profile";

/*
  Generated class for the ProfileServicesShow page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile-services-show',
  templateUrl: 'profile-services-show.html'
})
export class ProfileServicesShowPage {

  hospital;
  h;
  doctor;
  service;
  profile:ProfilePage;
  constructor(public navCtrl: NavController, public modalCtrl:ModalController, public http:Http, public params:NavParams) {
    let data = params.data;
    this.service = data.service;
    this.doctor = data.doctor;
    this.h = data.h;
    this.hospital = data.hospital;
    this.profile = data.profile;
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileServicesShowPage');
  }

  openAppointment(){

    this.navCtrl.pop().then();
    this.navCtrl.pop().then();
    console.log(this.h);
    console.log(this.service);
    this.profile.openAppointment(this.h,null, this.service);
  }

}
