"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var profile_services_show_1 = require("./profile-services-show/profile-services-show");
/*
  Generated class for the ProfileServices page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var ProfileServicesPage = (function () {
    function ProfileServicesPage(navCtrl, modalCtrl, http, params) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.http = http;
        this.params = params;
        var data = params.data;
        this.services = data.hospital.services;
        this.doctor = data.hospital.doctor;
        this.hospital = data.hospital.hospital;
        this.h = data.h;
        this.profile = data.profile;
    }
    ProfileServicesPage.prototype.open = function (service) {
        this.navCtrl.push(profile_services_show_1.ProfileServicesShowPage, {
            service: service,
            doctor: this.doctor,
            hospital: this.hospital,
            h: this.h,
            profile: this.profile
        }).then(null);
    };
    return ProfileServicesPage;
}());
ProfileServicesPage = __decorate([
    core_1.Component({
        selector: 'page-profile-services',
        templateUrl: 'profile-services.html'
    })
], ProfileServicesPage);
exports.ProfileServicesPage = ProfileServicesPage;
