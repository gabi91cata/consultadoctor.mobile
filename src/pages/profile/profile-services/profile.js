var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavParams, NavController, ModalController, LoadingController } from 'ionic-angular';
import { ProfileServicesPage } from "./profile-services/profile-services";
import { Http } from "@angular/http";
import { ProfileImagesPage } from "./profile-images/profile-images";
import { PhotoViewer } from "ionic-native";
import { ProfileMapPage } from "./profile-map/profile-map";
import { Doctor } from "../../eloquent/Doctor";
import { AppointmentPage } from "../appointment/appointment";
/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
export var ProfilePage = (function () {
    function ProfilePage(navCtrl, params, http, modalCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.params = params;
        this.http = http;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.pages = {
            images: ProfileImagesPage,
            map: ProfileMapPage,
            services: ProfileServicesPage
        };
        this.doctor = params.data;
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        new Doctor().find(this.doctor.id).later(function (a) {
            _this.doctor = a;
        });
    };
    ProfilePage.prototype.openPage = function (page, params) {
        this.navCtrl.push(page, params);
    };
    ProfilePage.prototype.openAppointment = function (app) {
        if (app === void 0) { app = null; }
        var profileModal = this.modalCtrl.create(AppointmentPage, { service: this.doctor.free.service, doctor: this.doctor, data: app });
        profileModal.present();
    };
    ProfilePage.prototype.presentModal = function (page, params) {
        PhotoViewer.show("https://adoclive.com/" + params.image.src);
        //let modal = this.modalCtrl.create(page,params);
        //modal.present();
    };
    ProfilePage = __decorate([
        Component({
            selector: 'page-profile',
            templateUrl: 'profile.html'
        }), 
        __metadata('design:paramtypes', [NavController, NavParams, Http, ModalController, LoadingController])
    ], ProfilePage);
    return ProfilePage;
}());
//# sourceMappingURL=profile.js.map