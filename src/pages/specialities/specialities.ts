import {Component} from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';
import {Search} from "../../eloquent/Search";

/*
 Generated class for the Specialities page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-specialities',
  templateUrl: 'specialities.html'
})
export class SpecialitiesPage {
  loading;
  query = '';
  result;
  search;
  variables;
  title;
  type;

  constructor(public viewCtrl: ViewController, public navParams: NavParams) {
    this.search = this.navParams.data.search;
    this.variables = this.navParams.data.variables;
    this.title = this.navParams.data.title;
    this.type = this.navParams.data.type;
    this.onInput(null);
  }

  ionViewDidLoad() {
  }

  onInput($event) {

    this.loading = true;
    if (this.variables)
      this.result = this.variables[this.type].data.filter(item => {
        return item.name.toLowerCase().indexOf(this.query.toLowerCase()) != -1;
      })
  }

  back() {
    this.viewCtrl.dismiss(null);
  }

  select(item) {
    if (this.type == 'specialities') {
      if (item.id)
        this.search.speciality = item.id;
      else delete this.search.speciality;
    }
    if (this.type == 'counties') {
      if (item.id)

        this.search.county = item.id;
      else delete this.search.county;
    }
    this.viewCtrl.dismiss(this.search);

  }

}
