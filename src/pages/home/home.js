"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var search_1 = require("../search/search");
require("rxjs/add/operator/map");
var Global_1 = require("../../modules/Global");
var Auth_1 = require("../../modules/Auth");
var HidingHeader_1 = require("../../modules/HidingHeader");
var Appointment_1 = require("../../eloquent/Appointment");
var appointments_details_1 = require("../appointments/appointments-details/appointments-details");
/*
 Generated class for the Home page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var HomePage = (function (_super) {
    __extends(HomePage, _super);
    function HomePage(navCtrl, modalCtrl, platform, http) {
        var _this = _super.call(this) || this;
        _this.navCtrl = navCtrl;
        _this.modalCtrl = modalCtrl;
        _this.platform = platform;
        _this.http = http;
        _this.pages = {
            search: search_1.SearchPage
        };
        _this.appointments = [];
        _this.user = Auth_1.Auth.user();
        _this.isRootPage = true;
        _this.welcomeUser();
        return _this;
        // this.slides.slidesPerView = 'auto';
    }
    HomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        _super.prototype.onScroll.call(this, this.content);
        setTimeout(function () {
            new Appointment_1.Appointment().with({ future: true }).get().then(function (response) {
                _this.appointments = response;
            });
        }, 1200);
    };
    HomePage.prototype.openAppointment = function (item) {
        this.navCtrl.push(appointments_details_1.AppointmentsDetailsPage, item).catch(function () {
        });
    };
    HomePage.prototype.welcomeUser = function () {
        var user = this.user;
        if (user)
            Global_1.Global.showMessage("Bine Ai revenit " + user.first_name + " " + user.last_name + ". Nu uita de sanatatea ta. Fa-ti o programare online acum!", user.profile, 1000);
    };
    HomePage.prototype.openPage = function (page) {
        Global_1.Global.stopMessages();
        this.navCtrl.push(page).catch(function () { });
    };
    return HomePage;
}(HidingHeader_1.HidingHeader));
__decorate([
    core_1.ViewChild('content')
], HomePage.prototype, "content", void 0);
__decorate([
    core_1.ViewChild('slider')
], HomePage.prototype, "slides", void 0);
HomePage = __decorate([
    core_1.Component({
        selector: 'page-home',
        templateUrl: 'home.html'
    })
], HomePage);
exports.HomePage = HomePage;
