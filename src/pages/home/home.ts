import {Component, ViewChild} from '@angular/core';
import {
  NavController, Platform, Slides, Content, ModalController
} from 'ionic-angular';
import {SearchPage} from "../search/search";
import {Http} from "@angular/http";

import 'rxjs/add/operator/map';
import {Global} from "../../modules/Global";
import {Auth} from "../../modules/Auth";
import {User} from "../../eloquent/User"
import {HidingHeader} from "../../modules/HidingHeader";
import {Appointment} from "../../eloquent/Appointment";
import {AppointmentsDetailsPage} from "../appointments/appointments-details/appointments-details";
/*
 Generated class for the Home page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage extends HidingHeader{

  @ViewChild('content') content:Content;
  pages = {
    search: SearchPage
  };

  appointments:any = [];
  user: User = Auth.user();
  isRootPage = true;
  @ViewChild('slider') slides: Slides;

  constructor(public navCtrl: NavController, private modalCtrl:ModalController,  public platform: Platform, public http: Http) {
    super();
    this.welcomeUser();

   // this.slides.slidesPerView = 'auto';
  }
  ionViewDidEnter()
  {
    super.onScroll(this.content);
    setTimeout(()=>{
      new Appointment().with({future:true}).get().then(response => {
        this.appointments = response;
      });
    },1200);
  }

  openAppointment(item)
  {
    this.navCtrl.push(AppointmentsDetailsPage, item).catch(() => {
    });
  }

  private welcomeUser()
  {
    let user = this.user;
    if(user)
      Global.showMessage("Bine Ai revenit " + user.first_name + " " + user.last_name + ". Nu uita de sanatatea ta. Fa-ti o programare online acum!", user.profile, 1000);

  }

  openPage(page) {
    Global.stopMessages()
    this.navCtrl.push(page).catch(() => {});
  }



}
