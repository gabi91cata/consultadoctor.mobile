"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ionic_native_1 = require("ionic-native");
var Search_1 = require("../../eloquent/Search");
var profile_1 = require("../profile/profile");
var Doctor_1 = require("../../eloquent/Doctor");
var results_1 = require("../results/results");
var SearchPage = (function () {
    function SearchPage(navCtrl, platform, navParams, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.loadingCtrl = loadingCtrl;
        this.query = "";
        this.search = new Search_1.Search();
        this.loading = false;
        this.focus = true;
    }
    SearchPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.query == "") {
                _this.searchBar.setFocus();
                ionic_native_1.Keyboard.show();
            }
            else {
                _this.onInput();
            }
        }, 10);
    };
    SearchPage.prototype.openAdvanced = function (data) {
        this.navCtrl.push(results_1.ResultsPage, data).catch(null);
    };
    SearchPage.prototype.openDoctor = function (doctor) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Va rugam asteptati....'
        });
        loading.present().catch(function () { });
        new Doctor_1.Doctor().find(doctor.id).later(function (doctor) {
            _this.navCtrl.push(profile_1.ProfilePage, doctor).then(function () {
                loading.dismiss().catch(function () { });
            }).catch(function (e) { });
        });
    };
    SearchPage.prototype.onInput = function () {
        var _this = this;
        this.result = null;
        this.loading = true;
        ionic_native_1.Keyboard.close();
        this.focus = false;
        Search_1.Search.Ajax(this.query).then(function (response) {
            _this.result = response;
            _this.loading = false;
        });
    };
    SearchPage.prototype.onCancel = function () {
        this.navCtrl.pop().catch(null);
    };
    return SearchPage;
}());
__decorate([
    core_1.ViewChild('searchBox')
], SearchPage.prototype, "searchBar", void 0);
SearchPage = __decorate([
    core_1.Component({
        selector: 'page-search',
        templateUrl: 'search.html',
    })
], SearchPage);
exports.SearchPage = SearchPage;
