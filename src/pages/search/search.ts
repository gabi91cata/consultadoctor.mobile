import {Component, ViewChild} from '@angular/core';
import {NavController, Platform, Searchbar, NavParams, LoadingController, IonicPage} from 'ionic-angular';
import { Keyboard} from 'ionic-native';
import {Search} from "../../eloquent/Search";
import {ProfilePage} from "../profile/profile";
import {Doctor} from "../../eloquent/Doctor";
import {ResultsPage} from "../results/results";


@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  @ViewChild('searchBox') searchBar: Searchbar;

  ionViewDidEnter() {
    setTimeout(() => {
      if(this.query == "")
      {
        this.searchBar.setFocus();
        Keyboard.show();
      }
      else {
        this.onInput();
      }

    }, 10);
  }

  query: string = "";
  result: any;
  search = new Search();
  loading = false;
  location;
  focus = true;


  constructor(public navCtrl: NavController, public platform: Platform, navParams:NavParams, public loadingCtrl: LoadingController) {


  }

  openAdvanced(data) {
    this.navCtrl.push(ResultsPage, data).catch(null);
  }




  openDoctor(doctor) {
    let loading = this.loadingCtrl.create({
      content: 'Va rugam asteptati....'
    });

    loading.present().catch(()=>{});

    new Doctor().find(doctor.id).later((doctor) => {

      this.navCtrl.push(ProfilePage, doctor).then(()=>{
        loading.dismiss().catch(()=>{});

      }).catch(e=>{});
    });
  }


  onInput() {

    this.result = null;
    this.loading = true;
    Keyboard.close();
    this.focus = false;

    Search.Ajax(this.query).then((response: any) => {
      this.result = response;
      this.loading = false;
    })
  }

  onCancel(){
    this.navCtrl.pop().catch(null);
  }

}
