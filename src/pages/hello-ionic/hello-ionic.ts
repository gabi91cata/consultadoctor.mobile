 import { Component } from '@angular/core';

 import {ToastController, Platform, NavController} from "ionic-angular";
 import {HomePage} from "../home/home";
 import {SearchPage} from "../search/search";

@Component({
  selector: 'page-hello-ionic',
  templateUrl: 'hello-ionic.html'
})
export class HelloIonicPage {

    home = HomePage;
    search = SearchPage;

  constructor(public toastCtrl: ToastController, public platform: Platform, navCtrl:NavController) {
    // let toast = this.toastCtrl.create({
    //   message: 'User was added successfully',
    //   duration: 3000
    // });
    // toast.present();


  }


}
