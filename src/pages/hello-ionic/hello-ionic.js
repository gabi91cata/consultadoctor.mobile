"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var home_1 = require("../home/home");
var search_1 = require("../search/search");
var HelloIonicPage = (function () {
    function HelloIonicPage(toastCtrl, platform, navCtrl) {
        // let toast = this.toastCtrl.create({
        //   message: 'User was added successfully',
        //   duration: 3000
        // });
        // toast.present();
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.home = home_1.HomePage;
        this.search = search_1.SearchPage;
    }
    return HelloIonicPage;
}());
HelloIonicPage = __decorate([
    core_1.Component({
        selector: 'page-hello-ionic',
        templateUrl: 'hello-ionic.html'
    })
], HelloIonicPage);
exports.HelloIonicPage = HelloIonicPage;
