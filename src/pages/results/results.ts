import {Component, Renderer2} from '@angular/core';
import {NavController, NavParams, PopoverController, ModalController, LoadingController} from 'ionic-angular';
import { StatusBar } from 'ionic-native';
import {Search} from "../../eloquent/Search";
import {ResultsFilterPage} from "./results-filter/results-filter";
import {ProfilePage} from "../profile/profile";
import {Doctor} from "../../eloquent/Doctor";

/*
  Generated class for the Results page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-results',
  templateUrl: 'results.html'
})
export class ResultsPage {

  result:any = {};
  doctors:any[] = [];
  loading = true;
  reachedEnd = false;
  filters;
  page = 1;
  constructor(public navCtrl: NavController,private renderer: Renderer2, private loadingCtrl:LoadingController, public navParams:NavParams, public modalCtrl: ModalController) {


    this.filters = navParams.data;
    this.getResults();
  }
  private lastSearch;

  getResults()
  {
    let data = this.filters;
    let search = new Search();
    return Search.Url(data).then((a: any) => {
      this.lastSearch = a;

      return this.getDoctors(a);
    })
  }

  getDoctors(a){
    let search = new Search();

    return search.with({id: a.id}).get(this.page).then((results:any) => {
      this.result = results;
      this.loading = false;

      this.result.results.data.forEach(i=>this.doctors.push(i));
      if(this.page < results.results.last_page)
        this.page = results.results.current_page + 1;
      else  this.reachedEnd = true;
    });
  }

  doInfinite(infiniteScroll) {


    if(this.reachedEnd){
      infiniteScroll.complete();
      return;
    }
    this.getDoctors(this.lastSearch).then(()=>{
      infiniteScroll.complete();
    });


  }

  openDoctor(doctor) {
    let loading = this.loadingCtrl.create({
      content: 'Va rugam asteptati....'
    });

    loading.present().catch(()=>{});

    new Doctor().find(doctor.id).later((doctor) => {

      this.navCtrl.push(ProfilePage, doctor).then(()=>{
        loading.dismiss().catch(()=>{});

      }).catch(e=>{});
    });
  }


  action(event) {
    if (event.value) {
      this.renderer.addClass(event.target, 'active');
      this.renderer.removeClass(event.target, 'inactive');
    } else {
      this.renderer.addClass(event.target, 'inactive');
      this.renderer.removeClass(event.target, 'active');
    }
  }



  presentPopover() {
    let popover = this.modalCtrl.create(ResultsFilterPage, this.result.search);
    popover.present();
    popover.onDidDismiss((s)=>{
      this.filters = s;
      this.page = 1;
      this.loading = true;
      this.reachedEnd = false;
      this.doctors = [];
      this.getResults();
    })
  }



}
