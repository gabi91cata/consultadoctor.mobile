"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Search_1 = require("../../eloquent/Search");
var results_filter_1 = require("./results-filter/results-filter");
var profile_1 = require("../profile/profile");
var Doctor_1 = require("../../eloquent/Doctor");
/*
  Generated class for the Results page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var ResultsPage = (function () {
    function ResultsPage(navCtrl, renderer, loadingCtrl, navParams, modalCtrl) {
        this.navCtrl = navCtrl;
        this.renderer = renderer;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.result = {};
        this.doctors = [];
        this.loading = true;
        this.reachedEnd = false;
        this.page = 1;
        this.filters = navParams.data;
        this.getResults();
    }
    ResultsPage.prototype.getResults = function () {
        var _this = this;
        var data = this.filters;
        var search = new Search_1.Search();
        return Search_1.Search.Url(data).then(function (a) {
            _this.lastSearch = a;
            return _this.getDoctors(a);
        });
    };
    ResultsPage.prototype.getDoctors = function (a) {
        var _this = this;
        var search = new Search_1.Search();
        return search.with({ id: a.id }).get(this.page).then(function (results) {
            _this.result = results;
            _this.loading = false;
            _this.result.results.data.forEach(function (i) { return _this.doctors.push(i); });
            if (_this.page < results.results.last_page)
                _this.page = results.results.current_page + 1;
            else
                _this.reachedEnd = true;
        });
    };
    ResultsPage.prototype.doInfinite = function (infiniteScroll) {
        if (this.reachedEnd) {
            infiniteScroll.complete();
            return;
        }
        this.getDoctors(this.lastSearch).then(function () {
            infiniteScroll.complete();
        });
    };
    ResultsPage.prototype.openDoctor = function (doctor) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Va rugam asteptati....'
        });
        loading.present().catch(function () { });
        new Doctor_1.Doctor().find(doctor.id).later(function (doctor) {
            _this.navCtrl.push(profile_1.ProfilePage, doctor).then(function () {
                loading.dismiss().catch(function () { });
            }).catch(function (e) { });
        });
    };
    ResultsPage.prototype.action = function (event) {
        if (event.value) {
            this.renderer.addClass(event.target, 'active');
            this.renderer.removeClass(event.target, 'inactive');
        }
        else {
            this.renderer.addClass(event.target, 'inactive');
            this.renderer.removeClass(event.target, 'active');
        }
    };
    ResultsPage.prototype.presentPopover = function () {
        var _this = this;
        var popover = this.modalCtrl.create(results_filter_1.ResultsFilterPage, this.result.search);
        popover.present();
        popover.onDidDismiss(function (s) {
            _this.filters = s;
            _this.page = 1;
            _this.loading = true;
            _this.reachedEnd = false;
            _this.doctors = [];
            _this.getResults();
        });
    };
    return ResultsPage;
}());
ResultsPage = __decorate([
    core_1.Component({
        selector: 'page-results',
        templateUrl: 'results.html'
    })
], ResultsPage);
exports.ResultsPage = ResultsPage;
