import { Component } from '@angular/core';
import {
  NavController, NavParams, ViewController, ModalController, LoadingController,
  AlertController
} from 'ionic-angular';
import {SpecialitiesPage} from "../../specialities/specialities";
import {Variable} from "../../../eloquent/Variable";
import {Search} from "../../../eloquent/Search";

/*
  Generated class for the ResultsFilter page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-results-filter',
  templateUrl: 'results-filter.html'
})
export class ResultsFilterPage {

  search;
  result;
  variables;
  loading=false;
  constructor(public navCtrl: NavController, private alertCtrl:AlertController, private loadingCtrl:LoadingController, private modalCtrl:ModalController, private viewCtrl:ViewController, public navParams: NavParams) {
    this.search = navParams.data.value;
    let loading = this.loadingCtrl.create({
      content: 'Va rugam asteptati....'
    });

    loading.present().catch(()=>{});

    new Variable().get().then(e=>{
      this.variables = e;
      loading.dismiss().then(null).catch(null);
    });
    this.getResults();
  }

  getResults(){
    let search = new Search();
    this.loading = true;
    return Search.Url(this.search).then((a: any) => {

      return search.with({id: a.id}).get(1).then((results:any) => {
        this.result = results;
        this.loading = false;

      });
    })



  }

  ionViewDidLoad() {
  }

  save(){
    this.viewCtrl.dismiss(this.search).then(null).catch(null);
  }
  reset(){
    this.search = {};
    this.getResults();
  }


  deleteFilter(f){
    this.search[f] = null;
  }

  specialityName(sp){
    if(this.variables)
      return this.variables.specialities.data.find((e)=>{return e.id==sp}).name;

  }
  countyName(sp){
    if(this.variables)
      return this.variables.counties.data.find((e)=>{return e.id==sp}).name;

  }


  openPage(title, type){
    let p = this.modalCtrl.create(SpecialitiesPage, {
      search:this.search,
      title: title,
      type:type,
      variables:this.variables
    });
    p.onDidDismiss((r)=>{
      if(r)
      this.search = r;
      this.getResults();

    });
    p.present();

  }


  selectType(){
    let alert = this.alertCtrl.create();
    alert.setTitle('Mod prezentare');

    alert.addInput({
      type: 'radio',
      label: 'Oricare',
      value: null,
      checked: !this.search.type
    });
    alert.addInput({
      type: 'radio',
      label: 'La cabinet cu plata',
      value: '1',
      checked: this.search.type == '1'
    });

    alert.addInput({
      type: 'radio',
      label: 'La cabinet cu bilet CNAS',
      value: '2',
      checked: this.search.type == '2'

    });
    alert.addInput({
      type: 'radio',
      label: 'Prin videoconferinta - Telemedicina',
      value: '3',
      checked: this.search.type == '3'

    });

    alert.addButton('Renunta');
    alert.addButton({
      text: 'Salveaza',
      handler: data => {
        this.search.type = data;
        this.getResults();

      }
    });
    alert.present();
  }
  selectService(){
    let prompt = this.alertCtrl.create({
      title: 'Serviciul dorit',
      inputs: [
        {
          name: 'service',
          placeholder: 'Serviciu',
          value: this.search.service
        },
      ],
      buttons: [
        {
          text: 'Renunta',
          handler: data => {
            console.log('Cancel clicked');
            this.getResults();

          }
        },
        {
          text: 'Salveaza',
          handler: data => {
            this.search.service = data.service;
            this.getResults();

          }
        }
      ]
    });
    prompt.present();
  }

  selectRating(){
    let alert = this.alertCtrl.create();
    alert.setTitle('Rating');

    alert.addInput({
      type: 'radio',
      label: 'Oricare',
      value: null,
      checked: !this.search.rating
    });
    alert.addInput({
      type: 'radio',
      label: '5 Stele',
      value: '5',
      checked: this.search.rating == '5'
    });
    alert.addInput({
      type: 'radio',
      label: '4 Stele',
      value: '4',
      checked: this.search.rating == '4'
    });

    alert.addInput({
      type: 'radio',
      label: '3 Stele',
      value: '3',
      checked: this.search.rating == '3'

    });
    alert.addInput({
      type: 'radio',
      label: '2 Stele',
      value: '2',
      checked: this.search.rating == '2'

    });
    alert.addInput({
      type: 'radio',
      label: 'o stea',
      value: '1',
      checked: this.search.rating == '1'

    });
    alert.addButton('Renunta');
    alert.addButton({
      text: 'Salveaza',
      handler: data => {
        this.search.rating = data;
        this.getResults();

      }
    });
    alert.present();
  }

}
