import { Component } from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';

/*
  Generated class for the AppointmentService page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-appointment-service',
  templateUrl: 'appointment-service.html'
})
export class AppointmentServicePage {

  title =  'Selecteaza serviciu';
  doctor;
  hospital;
  hospitals = [];
  constructor(public navCtrl: NavController, public viewCtrl:ViewController, public navParams: NavParams) {

    this.doctor = navParams.data.doctor;
    this.hospital = navParams.data.hospital;
    this.hospitals = this.doctor.hospitals.filter(item => item.hospital.id == this.hospital.id )
  }


  select(service)
  {
    this.viewCtrl.dismiss({service:service});
  }

}
