import { Component } from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';

/*
  Generated class for the AppointmentHospital page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-appointment-hospital',
  templateUrl: 'appointment-hospital.html'
})
export class AppointmentHospitalPage {

  doctor;
  free;
  constructor(public navCtrl: NavController, public viewCtrl:ViewController, public navParams: NavParams) {

    this.free = navParams.data.free;
    this.doctor = navParams.data.doctor;
  }


  select(hospital)
  {
    this.viewCtrl.dismiss({hospital: hospital}).catch(()=>{});
  }

}
