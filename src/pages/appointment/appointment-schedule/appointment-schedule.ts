import {Component, ViewChild} from '@angular/core';
import {NavController, NavParams, Slides, ViewController} from 'ionic-angular';
import {Doctor} from "../../../eloquent/Doctor";
import {ScheduleDoctor} from "../../../eloquent/ScheduleDoctor";

/*
 Generated class for the AppointmentSchedule page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
    selector: 'page-appointment-schedule',
    templateUrl: 'appointment-schedule.html'
})
export class AppointmentSchedulePage {
    @ViewChild('mySlider') slider: Slides;

    doctor: Doctor;
    schedule;
    dates = [];
    loading = false;

    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {

        let doctor = (this.navParams.data.doctor);
        this.doctor = doctor;
        let date = (this.navParams.data.data);

        this.getSchedule(date);

    }

    getSchedule(date) {
        let doctor = this.doctor;
      this.loading = true;


      let service = this.navParams.data.service;
        new ScheduleDoctor().with({
            doctor_id: doctor.id,
            date: date?date:new Date(),
            service: service.id,
            days: 9
        }).get().then((a: any) => {
            this.schedule = a;
            this.loading = false;

            this.dates.push(a.data.slice(0, 3));
            this.dates.push(a.data.slice(3, 6));
            this.dates.push(a.data.slice(6, 9));

            console.log('data', date);
            if(!date)
            {
              this.dates = [];
              this.getSchedule(new Date(a.next_free.hours[0]));
            }
        })
    }

    ionViewDidLoad() {

    }

    onSlideChanged() {
        let current = this.slider.getActiveIndex();

        let length = this.slider.length();
        if (length - current <= 2) {
            let dates = this.dates[this.dates.length - 1];
            let schedule = dates[dates.length - 1];
            console.log(schedule);
            this.getSchedule(schedule.date);
        }
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
    select(hour) {
        this.viewCtrl.dismiss(new Date(hour.date+" "+hour.hour));
    }

    forward()
    {
      this.slider.slideNext();
    }

    back()
    {
      this.slider.slidePrev();

    }
}
