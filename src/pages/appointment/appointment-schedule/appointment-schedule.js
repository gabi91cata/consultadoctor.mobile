"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ScheduleDoctor_1 = require("../../../eloquent/ScheduleDoctor");
/*
 Generated class for the AppointmentSchedule page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var AppointmentSchedulePage = (function () {
    function AppointmentSchedulePage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.dates = [];
        this.loading = false;
        var doctor = (this.navParams.data.doctor);
        this.doctor = doctor;
        var date = (this.navParams.data.data);
        this.getSchedule(date);
    }
    AppointmentSchedulePage.prototype.getSchedule = function (date) {
        var _this = this;
        var doctor = this.doctor;
        this.loading = true;
        var service = this.navParams.data.service;
        new ScheduleDoctor_1.ScheduleDoctor().with({
            doctor_id: doctor.id,
            date: date ? date : new Date(),
            service: service.id,
            days: 9
        }).get().then(function (a) {
            _this.schedule = a;
            _this.loading = false;
            _this.dates.push(a.data.slice(0, 3));
            _this.dates.push(a.data.slice(3, 6));
            _this.dates.push(a.data.slice(6, 9));
            console.log('data', date);
            if (!date) {
                _this.dates = [];
                _this.getSchedule(new Date(a.next_free.hours[0]));
            }
        });
    };
    AppointmentSchedulePage.prototype.ionViewDidLoad = function () {
    };
    AppointmentSchedulePage.prototype.onSlideChanged = function () {
        var current = this.slider.getActiveIndex();
        var length = this.slider.length();
        if (length - current <= 2) {
            var dates = this.dates[this.dates.length - 1];
            var schedule = dates[dates.length - 1];
            console.log(schedule);
            this.getSchedule(schedule.date);
        }
    };
    AppointmentSchedulePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AppointmentSchedulePage.prototype.select = function (hour) {
        this.viewCtrl.dismiss(new Date(hour.date + " " + hour.hour));
    };
    AppointmentSchedulePage.prototype.forward = function () {
        this.slider.slideNext();
    };
    AppointmentSchedulePage.prototype.back = function () {
        this.slider.slidePrev();
    };
    return AppointmentSchedulePage;
}());
__decorate([
    core_1.ViewChild('mySlider')
], AppointmentSchedulePage.prototype, "slider", void 0);
AppointmentSchedulePage = __decorate([
    core_1.Component({
        selector: 'page-appointment-schedule',
        templateUrl: 'appointment-schedule.html'
    })
], AppointmentSchedulePage);
exports.AppointmentSchedulePage = AppointmentSchedulePage;
