import { Component } from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';
import {Patient} from "../../../eloquent/Patient";

/*
  Generated class for the AppointmentPerson page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-appointment-person',
  templateUrl: 'appointment-person.html'
})
export class AppointmentPersonPage {

  patients:Array<Patient>;
  constructor(public navCtrl: NavController, public viewCtrl:ViewController, public navParams: NavParams) {
    this.patients = navParams.data.patients;
  }

  select(patient)
  {
    this.viewCtrl.dismiss(patient);
  }
}
