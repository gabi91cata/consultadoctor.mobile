"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ScheduleDoctor_1 = require("../../eloquent/ScheduleDoctor");
var appointment_schedule_1 = require("./appointment-schedule/appointment-schedule");
var login_1 = require("../login/login");
var Auth_1 = require("../../modules/Auth");
var appointment_person_1 = require("./appointment-person/appointment-person");
var Appointment_1 = require("../../eloquent/Appointment");
var Global_1 = require("../../modules/Global");
var appointment_hospital_1 = require("./appointment-hospital/appointment-hospital");
var appointment_service_1 = require("./appointment-service/appointment-service");
/*
 Generated class for the Appointment page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
var AppointmentPage = (function () {
    function AppointmentPage(navCtrl, loadingCtrl, navParams, modalCtrl, viewCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.user = Auth_1.Auth.user();
        this.appointment = new Appointment_1.Appointment();
        if (!this.user)
            this.openLogin();
        this.doctor = navParams.data.doctor;
        this.date = navParams.data.data;
        this.firstFree = navParams.data.firstFree;
        this.service = navParams.data.service;
        this.hospital = navParams.data.hospital;
        this.free = navParams.data.free;
        Auth_1.Auth.getUser().then(function (a) {
            _this.user = a;
            _this.person = _this.user.patients.find(function (patient) {
                return patient.default;
            });
        });
    }
    AppointmentPage.prototype.save = function () {
        var _this = this;
        var date = new Date(this.date);
        this.appointment.appointment_at = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes();
        this.appointment.details = {
            type: this.type,
            mobile: true,
            terms: true
        };
        this.appointment.patient = this.person;
        this.appointment.service = this.service;
        var loading = this.loadingCtrl.create({
            content: 'Salvez programarea...'
        });
        loading.present();
        this.appointment.save().then(function (a) {
            Global_1.Global.showMessage("Programarea ta a fost salvata la " + _this.doctor.name + ". Vezi aici detaliile", "https://consultadoctor.ro/" + _this.doctor.profile);
            loading.dismiss().catch(function () { });
            _this.viewCtrl.dismiss(a).catch(null);
        });
    };
    AppointmentPage.prototype.canSave = function () {
        return !!(this.doctor && this.date && this.service && this.person && this.type);
    };
    AppointmentPage.prototype.checkIfDateAvailable = function () {
        var _this = this;
        if (!this.date)
            return;
        var loading = this.loadingCtrl.create({
            content: 'Verific disponibilitate...'
        });
        loading.present().then(function () {
            new ScheduleDoctor_1.ScheduleDoctor().with({
                doctor_id: _this.doctor.id,
                date: new Date(new Date(_this.date).getTime() - 1000 * 60 * 60 * 24),
                service: _this.service.id,
                days: 1
            }).get().then(function (result) {
                try {
                    var hours = result.data[0].hours;
                    for (var _i = 0, hours_1 = hours; _i < hours_1.length; _i++) {
                        var hour = hours_1[_i];
                        var date = new Date(hour.date + ' ' + hour.hour + ':00');
                        if (date.getUTCDate() == new Date(_this.date).getUTCDate()) {
                            if (loading)
                                loading.dismiss().catch(function () {
                                });
                        }
                        else {
                            alert('hour not available anymore');
                        }
                    }
                    _this.checkType();
                    loading.dismiss().catch(function () {
                    });
                }
                catch (e) {
                    loading.dismiss().catch(function () {
                    });
                }
            });
        });
    };
    AppointmentPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        setTimeout(function () {
            _this.checkIfDateAvailable();
        }, 500);
        if (!this.date) {
            this.changeDate();
        }
    };
    AppointmentPage.prototype.changeDate = function () {
        var _this = this;
        var data = null;
        if (this.firstFree)
            data = new Date(new Date(this.firstFree).getTime() - 1000 * 60 * 60 * 24);
        var profileModal = this.modalCtrl.create(appointment_schedule_1.AppointmentSchedulePage, {
            service: this.service,
            doctor: this.doctor,
            data: data
        });
        profileModal.onDidDismiss(function (data) {
            if (data)
                _this.date = data;
            _this.checkType();
        });
        this.checkType();
        profileModal.present();
    };
    AppointmentPage.prototype.changeLocation = function () {
        var _this = this;
        var profileModal = this.modalCtrl.create(appointment_hospital_1.AppointmentHospitalPage, {
            doctor: this.doctor,
            free: this.free
        });
        profileModal.onDidDismiss(function (d) {
            if (d) {
                var data = d.hospital;
                _this.hospital = data.hospital.hospital;
                _this.firstFree = data.hours[0];
                _this.checkIfServiceAvailable(data.hospital, data.service);
                _this.checkIfDateAvailable();
            }
        });
        profileModal.present();
    };
    AppointmentPage.prototype.checkIfServiceAvailable = function (hospital, service) {
        var _this = this;
        var services = hospital.doctor.services.filter(function (item) { return item.doctor_hospital_id == hospital.id; }).filter(function (item) { return item.name == _this.service.name; });
        if (services.length)
            this.service = services[0];
        else {
            Global_1.Global.showMessage("Nu am gasit serviciul " + this.service.name + " in sediul " + this.hospital.name);
            this.service = service;
            this.changeService();
        }
        this.checkType();
    };
    AppointmentPage.prototype.checkType = function () {
        var nr = 0;
        var type = 0;
        if (this.service.insurance_house_id && this.doctor.hospital.name != 'telemedicine') {
            nr++;
            type = 2;
        }
        if (this.doctor.hospital.name != 'telemedicine') {
            nr++;
            type = 1;
        }
        if (this.service.accept_video) {
            nr++;
            type = 3;
        }
        if (nr == 1) {
            this.type = type;
        }
        console.log(nr);
    };
    AppointmentPage.prototype.changeService = function () {
        var _this = this;
        var profileModal = this.modalCtrl.create(appointment_service_1.AppointmentServicePage, {
            doctor: this.doctor,
            hospital: this.hospital
        });
        profileModal.onDidDismiss(function (d) {
            if (d) {
                _this.service = d.service;
                _this.checkIfDateAvailable();
            }
        });
        profileModal.present();
    };
    AppointmentPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AppointmentPage.prototype.selectPerson = function (me) {
        var _this = this;
        if (me === void 0) { me = false; }
        var profileModal = this.modalCtrl.create(appointment_person_1.AppointmentPersonPage, {
            patients: this.user.patients
        });
        profileModal.onDidDismiss(function (data) {
            _this.person = data;
        });
        profileModal.present();
    };
    AppointmentPage.prototype.openLogin = function () {
        var _this = this;
        var profileModal = this.modalCtrl.create(login_1.LoginPage);
        profileModal.onDidDismiss(function (data) {
            if (!data)
                _this.viewCtrl.dismiss(false);
        });
        profileModal.present();
    };
    return AppointmentPage;
}());
AppointmentPage = __decorate([
    core_1.Component({
        selector: 'page-appointment',
        templateUrl: 'appointment.html'
    })
], AppointmentPage);
exports.AppointmentPage = AppointmentPage;
