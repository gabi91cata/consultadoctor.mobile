import {Component} from '@angular/core';
import {
  NavController, NavParams, ViewController, ModalController,
  LoadingController
} from 'ionic-angular';
import {ScheduleDoctor} from "../../eloquent/ScheduleDoctor";
import {Doctor} from "../../eloquent/Doctor";
import {AppointmentSchedulePage} from "./appointment-schedule/appointment-schedule";
import {LoginPage} from "../login/login";
import {Auth} from "../../modules/Auth";
import {User} from "../../eloquent/User";
import {Patient} from "../../eloquent/Patient";
import {AppointmentPersonPage} from "./appointment-person/appointment-person";
import {Appointment} from "../../eloquent/Appointment";
import {Global} from "../../modules/Global";
import {AppointmentHospitalPage} from "./appointment-hospital/appointment-hospital";
import {AppointmentServicePage} from "./appointment-service/appointment-service";
import {AppointmentsDetailsPage} from "../appointments/appointments-details/appointments-details";

/*
 Generated class for the Appointment page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-appointment',
  templateUrl: 'appointment.html'
})
export class AppointmentPage {
  doctor: Doctor;
  date: Date;
  service;
  hospital;
  person: Patient;
  user: User = Auth.user();
  type;
  firstFree;
  free;
  appointment: Appointment = new Appointment();

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public navParams: NavParams, public modalCtrl: ModalController, public viewCtrl: ViewController) {
    if (!this.user)
      this.openLogin();

    this.doctor = navParams.data.doctor;
    this.date = navParams.data.data;
    this.firstFree = navParams.data.firstFree;
    this.service = navParams.data.service;
    this.hospital = navParams.data.hospital;
    this.free = navParams.data.free;

    Auth.getUser().then((a: any) => {
      this.user = a;
      this.person = this.user.patients.find((patient) => {
        return patient.default
      });
    });
  }


  save() {
    let date = new Date(this.date);

    this.appointment.appointment_at = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes();
    this.appointment.details = {
      type: this.type,
      mobile: true,
      terms: true
    };
    this.appointment.patient = this.person;
    this.appointment.service = this.service;

    let loading = this.loadingCtrl.create({
      content: 'Salvez programarea...'
    });
    loading.present();

    this.appointment.save().then((a) => {
      Global.showMessage("Programarea ta a fost salvata la " + this.doctor.name + ". Vezi aici detaliile", "https://consultadoctor.ro/" + this.doctor.profile);
      loading.dismiss().catch(()=>{});
      this.viewCtrl.dismiss(a).catch(null);

      });

  }


  canSave(): boolean {
    return !!(this.doctor && this.date && this.service && this.person && this.type);
  }

  checkIfDateAvailable() {
    if(!this.date)
      return;
    let loading = this.loadingCtrl.create({
      content: 'Verific disponibilitate...'
    });
    loading.present().then(() => {
      new ScheduleDoctor().with({
        doctor_id: this.doctor.id,
        date: new Date(new Date(this.date).getTime() - 1000 * 60 * 60 * 24),
        service: this.service.id,
        days: 1
      }).get().then((result: any) => {
        try {
          let hours = result.data[0].hours;
          for (let hour of hours) {
            let date = new Date(hour.date + ' ' + hour.hour + ':00');
            if (date.getUTCDate() == new Date(this.date).getUTCDate()) {
              if (loading)
                loading.dismiss().catch(() => {
                });
            }
            else {
              alert('hour not available anymore');
            }
          }
          this.checkType();


          loading.dismiss().catch(() => {
          });
        } catch (e) {
          loading.dismiss().catch(() => {
          });
        }
      })
    });





  }


  ionViewDidLoad() {
    setTimeout(() => {
      this.checkIfDateAvailable();
    }, 500)

    if (!this.date) {
      this.changeDate();
    }
  }

  changeDate() {

    let data = null;

    if(this.firstFree)
      data = new Date(new Date(this.firstFree).getTime() - 1000 * 60 * 60 * 24);

    let profileModal = this.modalCtrl.create(AppointmentSchedulePage, {
      service: this.service,
      doctor: this.doctor,
      data: data
    });
    profileModal.onDidDismiss(data => {
      if (data)
        this.date = data;
      this.checkType();

    });
    this.checkType();

    profileModal.present();
  }



  changeLocation() {
    let profileModal = this.modalCtrl.create(AppointmentHospitalPage, {
      doctor: this.doctor,
      free:this.free
    });
    profileModal.onDidDismiss(d => {
      if (d) {
        let data = d.hospital;
        this.hospital = data.hospital.hospital;
        this.firstFree = data.hours[0];
        this.checkIfServiceAvailable(data.hospital, data.service);

        this.checkIfDateAvailable();
      }
    });
    profileModal.present();
  }

  checkIfServiceAvailable(hospital, service)
  {
    let services = hospital.doctor.services.filter(item => item.doctor_hospital_id == hospital.id).filter(item=>item.name == this.service.name);
    if(services.length)
      this.service = services[0];
    else
    {
      Global.showMessage("Nu am gasit serviciul "+this.service.name+" in sediul "+this.hospital.name);
      this.service = service;
      this.changeService();
    }
    this.checkType();
  }

  checkType()
  {

    let nr = 0;
    let type = 0;
    if(this.service.insurance_house_id && this.doctor.hospital.name!='telemedicine')
    {
      nr++;
      type = 2;
    }
    if(this.doctor.hospital.name!='telemedicine')
    {
      nr++;
      type = 1;
    }
    if(this.service.accept_video)
    {
      nr++;
      type = 3;
    }
    if(nr == 1)
    {
      this.type = type;
    }
    console.log(nr);
  }

  changeService() {
    let profileModal = this.modalCtrl.create(AppointmentServicePage, {
      doctor: this.doctor,
      hospital:this.hospital
    });
    profileModal.onDidDismiss(d => {
      if (d) {
        this.service = d.service;
        this.checkIfDateAvailable();
      }
    });
    profileModal.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  selectPerson(me = false) {

    let profileModal = this.modalCtrl.create(AppointmentPersonPage, {
      patients: this.user.patients
    });
    profileModal.onDidDismiss(data => {
      this.person = data;
    });
    profileModal.present();
  }


  openLogin() {

    let profileModal = this.modalCtrl.create(LoginPage);
    profileModal.onDidDismiss(data => {
      if (!data)
        this.viewCtrl.dismiss(false);
    });
    profileModal.present();
  }


}
