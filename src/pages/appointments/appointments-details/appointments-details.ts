
import {Component, ViewChild} from '@angular/core';
import {NavController, NavParams, Content, LoadingController, ViewController, AlertController} from 'ionic-angular';
import {Appointment} from "../../../eloquent/Appointment";
import {HidingHeader} from "../../../modules/HidingHeader";
import {ProfilePage} from "../../profile/profile";
import {Doctor} from "../../../eloquent/Doctor";
import {Auth} from "../../../modules/Auth";



/*
 Generated class for the AppointmentsDetails page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-appointments-details',
  templateUrl: 'appointments-details.html'
})
export class AppointmentsDetailsPage extends HidingHeader {
  @ViewChild("content") content: Content;


  user;
  btree;
  seconds_to_pay;
  appointment: Appointment;

  constructor(public navCtrl: NavController, private alertCtrl:AlertController, public viewCtrl:ViewController, public navParams: NavParams, public loadingCtrl: LoadingController) {
    super();
    this.btree = window['BraintreePlugin'];

    this.user = Auth.user();
    this.appointment = new Appointment(navParams.data).find(navParams.data.id).later(()=>{

    });

    this.startCountDown();
  }

  reloadAppointment(id){
    new Appointment().find(id).later((a)=>{
      this.appointment = a;
    });
  }
  ionViewDidEnter() {
    super.onScroll(this.content);
  }

  viewDoctor() {
    let loading = this.loadingCtrl.create({
      content: "Se incarca..."
    });

    loading.present();
    let doctor = new Doctor().find(this.appointment.doctor_hospital.doctor.id).later(() => {


      this.navCtrl.push(ProfilePage, doctor).then(() => {
        loading.dismiss().catch(() => {
        })
      }).catch(() => {
      })
    });
  }

  private startCountDown()
  {



    this.seconds_to_pay = new Date(2017,10, 10, 0,0,0);
    this.seconds_to_pay = new Date(this.seconds_to_pay.getTime() + this.appointment.seconds_to_pay * 1000)

    if(this.appointment.seconds_to_pay <=0 )
      return;
    let interval = setInterval(()=>{
        this.seconds_to_pay = new Date(this.seconds_to_pay.getTime() - 1000)
        this.appointment.seconds_to_pay--;

        if(this.appointment.seconds_to_pay <= 0)
        {
          clearInterval(interval);
         // this.viewCtrl.dismiss().catch(null);
        }
    }, 1000);

  }


  pay()
  {

    this.btree.initialize(this.user.bt_token, (result) => {

      if (!result.userCancelled) {

        this.appointment.nonce = result.nonce;
        this.appointment.amount = this.appointment.service.price;
        this.appointment.update();

      }
    }, (e)=>{console.error(e)});


  }

  cancel()
  {

    let confirm = this.alertCtrl.create({
      title: 'Anulare programare',
      message: 'Esti sigur ca doresti sa anulezi aceasta programare?',
      buttons: [
        {
          text: 'nu',
          handler: () => {

          }
        },
        {
          text: 'DA',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Anulez programarea...'
            });
            loading.present();
            this.appointment.remove().then(()=>{
              loading.dismiss().catch(null);

                loading = this.loadingCtrl.create({
                content: 'Programare anulata'
              });

              this.viewCtrl.dismiss().catch(null);
            });

          }
        }
      ]
    });
    confirm.present();



  }

}
