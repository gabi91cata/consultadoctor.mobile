import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {Appointment} from "../../eloquent/Appointment";
import {Global} from "../../modules/Global";
import {SearchPage} from "../search/search";
import {AppointmentsDetailsPage} from "./appointments-details/appointments-details";

/*
 Generated class for the Appointments page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-appointments',
  templateUrl: 'appointments.html'
})
export class AppointmentsPage {

  appointments: any = [];
  withVar = {future: true};
  tabs = 'future';
  loading = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    Global.stopMessages();
  }

  ionViewDidLoad(refresher) {
    new Appointment().with(this.withVar).get().then(response => {
      this.appointments = response;
      if (refresher) refresher.complete()
    });
  }

  ionViewDidEnter()
  {
    this.load(this.withVar);
  }
  load(withVar) {
    this.withVar = withVar;
    this.loading = true;
    new Appointment().with(withVar).get().then(response => {
      this.appointments = response;
      this.loading = false;
    });
  }

  select(appointment) {
    let ctrl = this.navCtrl.push(AppointmentsDetailsPage, appointment).then((a)=>{
      console.log(a);
    }).catch(() => {
    });
  }

  appoint() {
    this.navCtrl.pop().then(() => {
      this.navCtrl.push(SearchPage).catch(() => {
      });
    })
  }

}
