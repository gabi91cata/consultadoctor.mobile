import {Component} from '@angular/core';
import {NavParams, ViewController} from 'ionic-angular';

/*
 Generated class for the Settings page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
    selector: 'page-settings',
    templateUrl: 'settings.html'
})
export class SettingsPage {

    constructor(public viewCtrl: ViewController, public navParams: NavParams) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SettingsPage');
    }

    dismiss($event) {
        $event.preventDefault();
        $event.stopPropagation();
        this.viewCtrl.dismiss();
    }
}
