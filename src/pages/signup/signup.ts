import {Component, ViewChild} from '@angular/core';
import {NavController, NavParams, ViewController, MenuController, ModalController, Slides} from 'ionic-angular';
import {Global} from "../../modules/Global";
import {Auth} from "../../modules/Auth";
import {User} from "../../eloquent/User";

/*
  Generated class for the Signup page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  @ViewChild(Slides) slides: Slides;

  login;
  constructor(public navCtrl: NavController, private modalCtrl:ModalController, public menuCtrl:MenuController, public navParams: NavParams, public viewCtrl: ViewController) {

    this.login = new Auth();
  }
  ngAfterViewInit() {
    this.slides.lockSwipeToNext(true);
    this.slides.lockSwipeToPrev(true);


  }
  submit()
  {
    this.slides.lockSwipeToNext(false);
    this.slides.slideNext();
    setTimeout(()=>{
      this.slides.lockSwipeToNext(true);
    },30);

  }
  dismiss()
  {
    this.viewCtrl.dismiss(false).then(()=>{});
  }

  signup() {

    let profileModal = this.modalCtrl.create(SignupPage);
    profileModal.onDidDismiss(data => {

    });

    profileModal.present().then(()=>{}).catch(e=>e.toString());

  }

}
