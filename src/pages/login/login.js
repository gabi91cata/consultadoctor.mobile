"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Global_1 = require("../../modules/Global");
var Auth_1 = require("../../modules/Auth");
var User_1 = require("../../eloquent/User");
var signup_1 = require("../signup/signup");
var LoginPage = (function () {
    function LoginPage(navCtrl, modalCtrl, menuCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.menuCtrl = menuCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.login = new Auth_1.Auth();
    }
    LoginPage.prototype.submit = function () {
        var _this = this;
        this.login.submit().then(function (user) {
            if (user) {
                if (user.gcm_token != Global_1.Global.gcm_token) {
                    var u = new User_1.User(user);
                    u.gcm_token = Global_1.Global.gcm_token;
                    u.with({ gcm: 1 }).save();
                }
                _this.viewCtrl.dismiss(false);
                _this.menuCtrl.close();
                Auth_1.Auth.setUser(user);
                Global_1.Global.showMessage("Bine ai venit " + user.first_name + " " + user.last_name + ".", user.profile);
            }
        });
    };
    LoginPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss(false).then(function () { }).catch();
    };
    LoginPage.prototype.signup = function () {
        var profileModal = this.modalCtrl.create(signup_1.SignupPage);
        profileModal.onDidDismiss(function (data) {
        });
        profileModal.present().then(function () { }).catch(function (e) { return e.toString(); });
    };
    return LoginPage;
}());
LoginPage = __decorate([
    core_1.Component({
        selector: 'page-login',
        templateUrl: 'login.html'
    })
], LoginPage);
exports.LoginPage = LoginPage;
