import {Component} from '@angular/core';
import {NavController, NavParams, ViewController, MenuController, ModalController, IonicPage} from 'ionic-angular';
import {Global} from "../../modules/Global";
import {Auth} from "../../modules/Auth";
import {User} from "../../eloquent/User";
import {SignupPage} from "../signup/signup";
import {FacebookLoginResponse, Facebook} from "@ionic-native/facebook";


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  login;

  constructor(public navCtrl: NavController, private fb: Facebook, private modalCtrl: ModalController, public menuCtrl: MenuController, public navParams: NavParams, public viewCtrl: ViewController) {

    this.login = new Auth();

  }

  submit() {
    this.login.submit().then(user => {
      if (user) {
        if (user.gcm_token != Global.gcm_token) {
          let u = new User(user);
          u.gcm_token = Global.gcm_token;
          u.with({gcm: 1}).save();
        }

        this.viewCtrl.dismiss(false);
        this.menuCtrl.close();
        Auth.setUser(user);
        Global.showMessage("Bine ai venit " + user.first_name + " " + user.last_name + ".", user.profile);

      }
    });
  }

  dismiss() {
    this.viewCtrl.dismiss(false).then(() => {
    }).catch();
  }

  signup() {

    let profileModal = this.modalCtrl.create(SignupPage);
    profileModal.onDidDismiss(data => {

    });

    profileModal.present().then(() => {
    }).catch(e => e.toString());

  }

  facebook() {

    this.fb.login(['public_profile', 'email', 'user_mobile_phone'])
      .then((res: FacebookLoginResponse) => console.log('Logged into Facebook!', res))
      .catch(e => console.log('Error logging into Facebook', e));
  }
}
