
import {Model} from "./Model";
import {Global} from "../modules/Global";
export class Doctor extends Model {
    protected $table = "doctor";
    protected $class = Doctor;
    public name;
    public id;
    public specialities;
    public description;
    public languages;
    public hospitals;
    public images;
    public settings;
    public following;
    public slug;
    public free;
    public profile;
    public hospital;

    public profile200()
    {
      return Global.url+this.profile+"-200.jpg";
    }


}
