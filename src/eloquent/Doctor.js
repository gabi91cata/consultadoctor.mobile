"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Model_1 = require("./Model");
var Global_1 = require("../modules/Global");
var Doctor = (function (_super) {
    __extends(Doctor, _super);
    function Doctor() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$table = "doctor";
        _this.$class = Doctor;
        return _this;
    }
    Doctor.prototype.profile200 = function () {
        return Global_1.Global.url + this.profile + "-200.jpg";
    };
    return Doctor;
}(Model_1.Model));
exports.Doctor = Doctor;
