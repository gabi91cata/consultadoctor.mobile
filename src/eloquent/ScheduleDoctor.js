"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Model_1 = require("./Model");
var ScheduleDoctor = (function (_super) {
    __extends(ScheduleDoctor, _super);
    function ScheduleDoctor() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$table = "schedule_doctor";
        _this.$class = ScheduleDoctor;
        _this.$dates = ['date'];
        return _this;
    }
    return ScheduleDoctor;
}(Model_1.Model));
exports.ScheduleDoctor = ScheduleDoctor;
