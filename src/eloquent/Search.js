"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Model_1 = require("./Model");
var Doctor_1 = require("./Doctor");
var Global_1 = require("../modules/Global");
var Search = (function (_super) {
    __extends(Search, _super);
    function Search() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$table = "searche";
        _this.$class = Search;
        _this.$with = {
            doctors: Doctor_1.Doctor
        };
        return _this;
    }
    Search.prototype.getSavedText = function () {
        return "Aceasta cautare a fost salvata";
    };
    Search.Url = function (data) {
        var promise = new Global_1.Deferred();
        Global_1.Global.$http.post(Global_1.Global.url + '/api/search', { value: data }).map(function (res) { return res.json(); }).subscribe(function (val) {
            promise.resolve(val);
        });
        return promise.promise;
    };
    Search.Ajax = function (data) {
        var promise = new Global_1.Deferred();
        Global_1.Global.$http.post(Global_1.Global.url + '/api/ajax-search', { value: data }).map(function (res) { return res.json(); }).subscribe(function (val) {
            promise.resolve(val);
        });
        return promise.promise;
    };
    Search.Search = function (type, data) {
        var promise = new Global_1.Deferred();
        Global_1.Global.$http.post(Global_1.Global.url + '/api/ajax-search', { type: type, value: data }).map(function (res) { return res.json(); }).subscribe(function (val) {
            promise.resolve(val);
        });
        return promise.promise;
    };
    return Search;
}(Model_1.Model));
exports.Search = Search;
