import {Global, Deferred} from "../modules/Global";
import {Base64} from "../modules/Base64";
import {RequestOptions} from "@angular/http";


export class Model {
    protected $table;
    protected $class = Model;
    protected $dates = [];
    protected $with = {};
    protected $errors = {};
    protected $deleteText;

    protected $cache: boolean = false;
    private saving = false;
    private deleting = false;
    public deleted = false;
    public loading = false;
    private attributes: any = {};
    private wheres = [];
    public listAll = [];
    private data = {};
    private options = new RequestOptions({withCredentials: true });

    constructor(attributes = {}) {

        this.attributes = attributes;
        for (let i in attributes) {
            this[i] = attributes[i];
        }
        delete(this.attributes);
        return this;
    }

    protected getDeletedText() {
        return "Datele au fost sterse cu succes!";
    }

    protected getSavedText() {
        return "Datele au fost salvate cu succes!";
    }

    protected getDenyText() {
        return "Nu ai drepturi necesare!";
    }

    /**
     * Get the model by id
     * @param id
     * @returns {Models.Model}
     */
    public find(id: number, objectify = true) {
        let where = "";
        if (this.data)
            where += '&data=' + Base64.encode(JSON.stringify(this.data));


        this.loading = true;

        Global.$http.get(Global.url+'/api/' + this.$table + '/' + id + "?" + where, this.options).map(res => res.json()).subscribe(val => {
            this.loading = false;
            this.constructor(val);
            this.resolveDates();

            if (this._then)
              if(objectify)
                this._then(this)
              else
                this._then(val);


            for (let l of this._thens) {
                l.cb(val);
            }

        });
        return this;
    }

    private _then;
    private _thens = [];

    public later(val) {
        this._then = val;
        return this;
    }

    public laters(val) {
        this._thens.push({cb: val});

        return this;
    }

    public reload(attributes) {
        this.attributes = attributes;
        for (let i in attributes) {
            this[i] = attributes[i];
        }
        delete(this.attributes);
        this.resolveDates();

    }

    /**
     * Delete current model
     * @returns {IHttpPromise<T>}
     */
    public remove($deleteText = null) {


        this.deleting = true;
        let d = new Deferred();
        Global.$http.delete(Global.url+'/api/' + this.$table + '/' + this['id'], this.options).map(res => res.json()).subscribe(data => {

            d.resolve();
            this.deleting = false;
            this.deleted = true;
        });
        return d.promise;

    }

    public getAll() {
        return this.listAll;
    }

    public save($text = null) {
        if (this['id'] && this['id'] != 'new')
            return this.update($text);

        let d = new Deferred();
        this.saving = true;
        this['_token'] = window['CSRF_TOKEN'];

        let where = "";
        if (this.data)
            where += '&data=' + Base64.encode(JSON.stringify(this.data));

        Global.$http.post(Global.url+'/api/' + this.$table + "?" + where, this, this.options).map(res => res.json()).subscribe(val => {

            d.resolve((new this.$class(val)).resolveDates());
            this.reload(val);
            this.saving = false;
            //Global.$mdToast.show($text ? $text : this.getSavedText());
        })
        return d.promise;
    }

    public update($text: string=""): any {
        if (!this['id'] || this['id'] == 'new')
            return this.save($text);
        let d = new Deferred();

        this.saving = true;
        let where = "";
        if (this.data)
            where += '&data=' + Base64.encode(JSON.stringify(this.data));

        Global.$http.put(Global.url+'/api/' + this.$table + '/' + this['id'] + "?" + where, this, this.options).map(res => res.json()).subscribe(val => {

            this.constructor(val);
            d.resolve(new this.$class(val).resolveDates());
            this.saving = false;
          //  Global.$mdToast.show(($text ? $text : this.getSavedText()));
        })

        return d.promise;
    }


    public with(data) {
        this.data = data;
        return this;
    }

    public where(...args: any[]) {
        switch (args.length) {
            case 1:
                throw Error('Must have at least 2 arguments');
            case 2:
                this.wheres.push({
                    key: args[0],
                    condition: '=',
                    value: args[1]
                });
                break;
            case 3:
                this.wheres.push({
                    key: args[0],
                    condition: args[1],
                    value: args[2]
                });
                break;

        }
        return this;
    }


    /**
     * Get all the models here
     * @param page
     * @returns {IPromise<T>}
     */
    public get(page = null) {


        let d = new Deferred();
        if (page != null)
            page = 'page=' + page;
        else
            page = '';
        let where = '';
        this.loading = true;
        if (this.wheres.length > 0)
            where = '&where=' + Base64.encode(JSON.stringify(this.wheres));
        if (this.data)
            where += '&data=' + Base64.encode(JSON.stringify(this.data));



        Global.$http.get(Global.url+'/api/' + this.$table + '?' + page + where, this.options).map(res => res.json()).subscribe(data => {

            let val = data;

            let loop = [];
            if (val.data)
                loop = val.data;
            else
                loop = val;


            if (loop[0]) {
                for (let i in loop)
                    loop[i] = (new this.$class(loop[i])).resolveDates();
            }


            for (let j in this.$with) {

                if (val[j] && !val[j].data) {
                    if (Object.prototype.toString.call(val[j]) === '[object Array]')
                        for (let i in val[j])
                            val[j][i] = (new this.$with[j](val[j][i])).resolveDates();
                    else
                        val[j] = (new this.$with[j](val[j])).resolveDates();
                }

                if (val[j] && val[j].data) {
                    if (Object.prototype.toString.call(val[j].data) === '[object Array]')
                        for (let i in val[j].data)
                            val[j].data[i] = (new this.$with[j](val[j].data[i])).resolveDates();


                }
            }


            this.listAll = val;
            if (val.data) {
                val.data = loop;
                d.resolve(val);
            }
            else
                d.resolve(loop);
            this.loading = false;


        });
        return d.promise;
    }

    public resolveDates() {
        let dates = this.$dates;
        dates.push('created_at');
        dates.push('updated_at');
        for (let i in this) {
            if (dates.indexOf(i) != -1) {
                if (this[i])
                    this[i.toString()] = new Date(this[i].toString());
            }
        }


        for (let j in this.$with) {

            if (this[j]) {
                if (Object.prototype.toString.call(this[j]) === '[object Array]')
                    for (let i in this[j])
                        this[j][i] = (new this.$with[j](this[j][i])).resolveDates();
                else
                    this[j] = (new this.$with[j](this[j])).resolveDates();


            }
        }

        return this;
    }

}
