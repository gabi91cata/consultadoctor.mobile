
import {Model} from "./Model";
import {User} from "./User";
import {DoctorHospital} from "./DoctorHospital";
import {Patient} from "./Patient";
import {Service} from "./Service";
export class Appointment extends Model {
    protected $table = "appointment";
    protected $class = Appointment;
    protected $dates = ['appointment_at'];
    protected $with = {'user':User, person:Patient, doctor_hospital:DoctorHospital, service:Service};
    public id;
    public user;
    public appointment_at;
    public service:Service;
    public details;
    public person:Patient;
    public doctor_hospital:DoctorHospital;
    public patient:Patient;

    public canceled:boolean;
    public can_pay:boolean;
    public can_cancel:boolean;
    public seconds_to_pay;
    public seconds;
  public client_token;
  public nonce;
  public amount;


}
