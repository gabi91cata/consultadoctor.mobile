import {Model} from "./Model";
export class Service extends Model {
  protected $table = "service";
  protected $class = Service;
  public name;
  public id;
  public price;
  public offer_price;
  public description;
  public duration:number;


}
