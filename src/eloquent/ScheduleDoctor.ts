
import {Model} from "./Model";

    export class ScheduleDoctor extends Model{
        protected $table = "schedule_doctor";
        protected $class = ScheduleDoctor;
        protected $dates = ['date'];
        public name;
        public date;
        public hours;

    }
