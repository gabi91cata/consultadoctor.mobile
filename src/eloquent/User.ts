
import {Model} from "./Model";
import {Doctor} from "./Doctor";
import {Patient} from "./Patient";
export class User extends Model {
    protected $table = "user";
    protected $class = User;
    protected $with = {'doctor': Doctor, 'patients':Patient};
    public name;
    public selected:boolean;
    public id;
    public first_name;
    public last_name;
    public email;
    public profile;
    public sip_username;
    public sip_password;
    public remember_token;
    public patients:Array<Patient>;
    public gcm_token;

}
