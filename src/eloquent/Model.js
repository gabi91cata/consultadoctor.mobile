"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Global_1 = require("../modules/Global");
var Base64_1 = require("../modules/Base64");
var http_1 = require("@angular/http");
var Model = (function () {
    function Model(attributes) {
        if (attributes === void 0) { attributes = {}; }
        this.$class = Model;
        this.$dates = [];
        this.$with = {};
        this.$errors = {};
        this.$cache = false;
        this.saving = false;
        this.deleting = false;
        this.deleted = false;
        this.loading = false;
        this.attributes = {};
        this.wheres = [];
        this.listAll = [];
        this.data = {};
        this.options = new http_1.RequestOptions({ withCredentials: true });
        this._thens = [];
        this.attributes = attributes;
        for (var i in attributes) {
            this[i] = attributes[i];
        }
        delete (this.attributes);
        return this;
    }
    Model.prototype.getDeletedText = function () {
        return "Datele au fost sterse cu succes!";
    };
    Model.prototype.getSavedText = function () {
        return "Datele au fost salvate cu succes!";
    };
    Model.prototype.getDenyText = function () {
        return "Nu ai drepturi necesare!";
    };
    /**
     * Get the model by id
     * @param id
     * @returns {Models.Model}
     */
    Model.prototype.find = function (id, objectify) {
        var _this = this;
        if (objectify === void 0) { objectify = true; }
        var where = "";
        if (this.data)
            where += '&data=' + Base64_1.Base64.encode(JSON.stringify(this.data));
        this.loading = true;
        Global_1.Global.$http.get(Global_1.Global.url + '/api/' + this.$table + '/' + id + "?" + where, this.options).map(function (res) { return res.json(); }).subscribe(function (val) {
            _this.loading = false;
            _this.constructor(val);
            _this.resolveDates();
            if (_this._then)
                if (objectify)
                    _this._then(_this);
                else
                    _this._then(val);
            for (var _i = 0, _a = _this._thens; _i < _a.length; _i++) {
                var l = _a[_i];
                l.cb(val);
            }
        });
        return this;
    };
    Model.prototype.later = function (val) {
        this._then = val;
        return this;
    };
    Model.prototype.laters = function (val) {
        this._thens.push({ cb: val });
        return this;
    };
    Model.prototype.reload = function (attributes) {
        this.attributes = attributes;
        for (var i in attributes) {
            this[i] = attributes[i];
        }
        delete (this.attributes);
        this.resolveDates();
    };
    /**
     * Delete current model
     * @returns {IHttpPromise<T>}
     */
    Model.prototype.remove = function ($deleteText) {
        var _this = this;
        if ($deleteText === void 0) { $deleteText = null; }
        this.deleting = true;
        var d = new Global_1.Deferred();
        Global_1.Global.$http.delete(Global_1.Global.url + '/api/' + this.$table + '/' + this['id'], this.options).map(function (res) { return res.json(); }).subscribe(function (data) {
            d.resolve();
            _this.deleting = false;
            _this.deleted = true;
        });
        return d.promise;
    };
    Model.prototype.getAll = function () {
        return this.listAll;
    };
    Model.prototype.save = function ($text) {
        var _this = this;
        if ($text === void 0) { $text = null; }
        if (this['id'] && this['id'] != 'new')
            return this.update($text);
        var d = new Global_1.Deferred();
        this.saving = true;
        this['_token'] = window['CSRF_TOKEN'];
        var where = "";
        if (this.data)
            where += '&data=' + Base64_1.Base64.encode(JSON.stringify(this.data));
        Global_1.Global.$http.post(Global_1.Global.url + '/api/' + this.$table + "?" + where, this, this.options).map(function (res) { return res.json(); }).subscribe(function (val) {
            d.resolve((new _this.$class(val)).resolveDates());
            _this.reload(val);
            _this.saving = false;
            //Global.$mdToast.show($text ? $text : this.getSavedText());
        });
        return d.promise;
    };
    Model.prototype.update = function ($text) {
        var _this = this;
        if ($text === void 0) { $text = ""; }
        if (!this['id'] || this['id'] == 'new')
            return this.save($text);
        var d = new Global_1.Deferred();
        this.saving = true;
        var where = "";
        if (this.data)
            where += '&data=' + Base64_1.Base64.encode(JSON.stringify(this.data));
        Global_1.Global.$http.put(Global_1.Global.url + '/api/' + this.$table + '/' + this['id'] + "?" + where, this, this.options).map(function (res) { return res.json(); }).subscribe(function (val) {
            _this.constructor(val);
            d.resolve(new _this.$class(val).resolveDates());
            _this.saving = false;
            //  Global.$mdToast.show(($text ? $text : this.getSavedText()));
        });
        return d.promise;
    };
    Model.prototype.with = function (data) {
        this.data = data;
        return this;
    };
    Model.prototype.where = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        switch (args.length) {
            case 1:
                throw Error('Must have at least 2 arguments');
            case 2:
                this.wheres.push({
                    key: args[0],
                    condition: '=',
                    value: args[1]
                });
                break;
            case 3:
                this.wheres.push({
                    key: args[0],
                    condition: args[1],
                    value: args[2]
                });
                break;
        }
        return this;
    };
    /**
     * Get all the models here
     * @param page
     * @returns {IPromise<T>}
     */
    Model.prototype.get = function (page) {
        var _this = this;
        if (page === void 0) { page = null; }
        var d = new Global_1.Deferred();
        if (page != null)
            page = 'page=' + page;
        else
            page = '';
        var where = '';
        this.loading = true;
        if (this.wheres.length > 0)
            where = '&where=' + Base64_1.Base64.encode(JSON.stringify(this.wheres));
        if (this.data)
            where += '&data=' + Base64_1.Base64.encode(JSON.stringify(this.data));
        Global_1.Global.$http.get(Global_1.Global.url + '/api/' + this.$table + '?' + page + where, this.options).map(function (res) { return res.json(); }).subscribe(function (data) {
            var val = data;
            var loop = [];
            if (val.data)
                loop = val.data;
            else
                loop = val;
            if (loop[0]) {
                for (var i in loop)
                    loop[i] = (new _this.$class(loop[i])).resolveDates();
            }
            for (var j in _this.$with) {
                if (val[j] && !val[j].data) {
                    if (Object.prototype.toString.call(val[j]) === '[object Array]')
                        for (var i in val[j])
                            val[j][i] = (new _this.$with[j](val[j][i])).resolveDates();
                    else
                        val[j] = (new _this.$with[j](val[j])).resolveDates();
                }
                if (val[j] && val[j].data) {
                    if (Object.prototype.toString.call(val[j].data) === '[object Array]')
                        for (var i in val[j].data)
                            val[j].data[i] = (new _this.$with[j](val[j].data[i])).resolveDates();
                }
            }
            _this.listAll = val;
            if (val.data) {
                val.data = loop;
                d.resolve(val);
            }
            else
                d.resolve(loop);
            _this.loading = false;
        });
        return d.promise;
    };
    Model.prototype.resolveDates = function () {
        var dates = this.$dates;
        dates.push('created_at');
        dates.push('updated_at');
        for (var i in this) {
            if (dates.indexOf(i) != -1) {
                if (this[i])
                    this[i.toString()] = new Date(this[i].toString());
            }
        }
        for (var j in this.$with) {
            if (this[j]) {
                if (Object.prototype.toString.call(this[j]) === '[object Array]')
                    for (var i in this[j])
                        this[j][i] = (new this.$with[j](this[j][i])).resolveDates();
                else
                    this[j] = (new this.$with[j](this[j])).resolveDates();
            }
        }
        return this;
    };
    return Model;
}());
exports.Model = Model;
