
import {Model} from "./Model";
export class Patient extends Model {
    protected $table = "patient";
    protected $class = Patient;
    public name;
    public id;
    public first_name;
    public last_name;
    public default:boolean;

}
