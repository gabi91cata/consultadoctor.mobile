


    import {Model} from "./Model";
    import {Doctor} from "./Doctor";
    import {Deferred, Global} from "../modules/Global";
    export class Search extends Model{
        protected $table = "searche";
        protected $class = Search;
        protected $with = {
            doctors: Doctor
        };
        public name;

        protected getSavedText()
        {
            return "Aceasta cautare a fost salvata";
        }


        static Url(data) {
            var promise = new Deferred();
            Global.$http.post(Global.url+'/api/search', {value:data}).map(res => res.json()).subscribe(val => {
                promise.resolve(val);
            });
            return promise.promise;
        }

      static Ajax(data) {
        var promise = new Deferred();
        Global.$http.post(Global.url+'/api/ajax-search', {value:data}).map(res => res.json()).subscribe(val => {
          promise.resolve(val);
        });

        return promise.promise;
      }
      static Search(type,data) {
        let promise = new Deferred();
        Global.$http.post(Global.url+'/api/ajax-search', {type:type, value:data}).map(res => res.json()).subscribe(val => {
          promise.resolve(val);
        });

        return promise.promise;
      }


    }
