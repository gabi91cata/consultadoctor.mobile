
import {Model} from "./Model";
import {User} from "./User";
export class Message extends Model{
  protected $table = "message";
  protected $class = Message;
  public id;
  public message;
  public type;
  public user_id;
  public channel_id;
  public created_at;
  public updated_at;

}
