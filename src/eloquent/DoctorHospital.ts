import {Model} from "./Model";
import {Doctor} from "./Doctor";
import {Hospital} from "./Hospital";
export class DoctorHospital extends Model {
  protected $table = "doctor_hospital";
  protected $class = DoctorHospital;
  public name;
  public id;
  public doctor: Doctor;
  public hospital: Hospital;


}
