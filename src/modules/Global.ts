import {Http} from "@angular/http";
import {ToastController, ModalController, Toast} from "ionic-angular";

export class Global {

  public static $http: Http;
  public static toast;
  public static gcm_token;
  public static modal: ModalController;
  public static isHome: boolean;
  public static url: string = "https://consultadoctor.ro";
  public static $mdToast = {
    show: (message, duration = 5000) => {
      let toast = Global.toast.create({
        message: message,
        duration: duration
      });
      toast.present();
    }
  };

  private static _messageTimeout;
  private static _messageToast: Toast;

  public static showMessage(string, image = null, delay = 100, position = 'bottom', duration = 6000) {
    this._messageToast = Global.toast.create({
      message: string,
      img: image,
      duration: duration,
      position: position
    });


    this._messageTimeout = setTimeout(() => {
      this._messageToast.present().then(() => {
      }).catch(() => {
      });
    }, delay)
  }

  public static stopMessages() {
    try {
      clearTimeout(this._messageTimeout);
      this._messageToast.dismiss().catch(() => {
      })

    } catch (e) {
    }
  }

  constructor(public http: Http, public toastCtrl: ToastController, public modal: ModalController) {

    Global.$http = http;
    Global.toast = toastCtrl;
    Global.modal = modal;

  }


  public static getDistance(p1, p2) {
    if(!p1 || !p2) return -1;
    let rad = (x) => {
      return x * Math.PI / 180;
    };
    let R = 6378137; // Earth’s mean radius in meter
    let dLat = rad(p2.lat - p1.lat);
    let dLong = rad(p2.lng - p1.lng);
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
      Math.sin(dLong / 2) * Math.sin(dLong / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return Math.round((R * c / 1000) * 100) / 100;
  }

}

export class Deferred<T> {

  promise: Promise<T>;
  resolve: (value?: T | PromiseLike<T>) => void;
  reject: (reason?: any) => void;

  constructor() {
    this.promise = new Promise<T>((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    });
  }
}
