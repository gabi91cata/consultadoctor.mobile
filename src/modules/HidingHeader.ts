
export class HidingHeader {

  onScroll(content) {

    content.ionScroll.subscribe((a) => {
      let percent = 0;
      if (a.contentTop >= a.scrollTop)
        percent = a.scrollTop * 100 / a.contentTop;
      else {
        a.headerElement.querySelector(".toolbar-background").style.opacity = 1;
        a.headerElement.classList.add('show-border');
        return;
      }

      a.headerElement.querySelector(".toolbar-background").style.opacity = percent / 100;
      a.headerElement.classList.remove('show-border');

    });
  }




}
