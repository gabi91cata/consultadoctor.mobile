import { Geolocation } from 'ionic-native';

export class GeoModule {

  public static coords;

  public static watchCoords(cb, timer)
  {
    var int = setInterval(()=>{
      if(GeoModule.coords)
      {
        cb(GeoModule.coords);
        clearInterval(int);
      }

    },100);
    var c;
    setInterval(()=>{

      if(c != GeoModule.coords)
      {
        cb(GeoModule.coords);
        c = (GeoModule.coords);
      }

    }, timer)
  }
  public static getCoords()
  {
    var latitude, longitude;

    let watch = Geolocation.watchPosition();
    var getCoordinates = (data:any) => {


      if(data.code != 2)
      {
        var lat1 =  parseFloat(data.coords.latitude.toFixed(6));
        var lon1 = parseFloat(data.coords.longitude.toFixed(6));

        if(latitude != lat1 || longitude != lon1)
        {
          latitude = lat1;
          longitude = lon1;
        }
        GeoModule.coords = ({
          lat:latitude,
          lng:longitude,
        });
      }
    };
    Geolocation.getCurrentPosition().then(getCoordinates).catch(()=>{});

    watch.subscribe(getCoordinates);

  }

}
