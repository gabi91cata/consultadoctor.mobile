import * as SIP from "sip.js";
import {Message} from "../eloquent/Message";
import {Auth} from "./Auth";
import {User} from "../eloquent/User";

export class Phone {


  private phone;
  private session;
  options;
  private user: User;
  public onRegister;
  public onEndCall;

  private _inCall: boolean = false;
  private _autoAnswer: boolean;

  private _mediaStream: MediaStream;
  private _constrains = {};

  constructor(private channel_id) {

    this.user = new User(Auth.user());

    MediaStreamTrack['getSources']((sourceInfos) => {
      let videoSourceId;
      for (let i = 0; i != sourceInfos.length; ++i) {
        let sourceInfo = sourceInfos[i];
        if (sourceInfo.kind == "video" && sourceInfo.facing == "environment") {
          this._constrains["back"] = {
            audio: true,
            video: {
              optional: [{sourceId: sourceInfo.id}]
            }
          };
        }else
        if (sourceInfo.kind == "video" && sourceInfo.facing == "user") {
          this._constrains["face"] = {
            audio: true,
            video: {
              optional: [{sourceId: sourceInfo.id}]
            }
          };
        }
      }

      let c = this._constrains['face'];
      if(!c)
        c = {audio:true, video:true};
      navigator['webkitGetUserMedia'](c, (stream: MediaStream) => {
        this._mediaStream = stream;
        this.init();

      }, () => {
      })

    });



  }

  public autoAnswer(_autoAnswer: boolean) {
    this._autoAnswer = _autoAnswer;
  }

  public sendMessage(type, message) {
    let m = new Message();
    m.channel_id = this.channel_id;
    m.type = type;
    m.message = message;
    m.user_id = this.user.id;
    m.save();
  }

  public inCall(): boolean {
    return this._inCall;
  }

  private init() {


    this.options = {
      media: {
        render: {
          remote: document.getElementById("remoteVideo"),
          local: document.getElementById("localVideo")
        },
        stream: this._mediaStream
      }
    };

    let configuration = {
      'wsServers': 'wss://sv.consultadoctor.ro:10443',
      'uri': 'sip:' + this.user.sip_username + '@sv.consultadoctor.ro',
      'authorizationUser': this.user.sip_username,
      'password': this.user.sip_password,
      stunServers: ["stun:stun2.l.google.com:19302"],
      turnServers: [
        {
          urls: 'turn:numb.viagenie.ca',
          password: 'adri91ana',
          username: 'gabi.brosteanu@gmail.com'
        },

      ],
      iceCheckingTimeout: 10000,
      hackIpInContact: true,
      'realm': 'sv.consultadoctor.ro',
      'register': true
    };
    this.phone = new SIP.UA(configuration);
    this.phone.on('invite', (e) => {
      if (e.method == "INVITE") {
        this.session = e;
        this.type = 'incoming';
        if (this._autoAnswer)
          this.answer();
        this.callbacks();

      }
    });

    this.phone.on('registered', this.onRegister)

  }

  public answer() {

    console.log("video", this._mediaStream.getVideoTracks());
    console.log("audio", this._mediaStream.getAudioTracks());
    this.session.accept(this.options);
    this.type = 'accepted';
    this._inCall = true;


  }

  private _frontCamera: boolean = true;

  public changeCamera() {

    let stream = this.session.getLocalStreams()[0];
    console.log("first ",stream.getVideoTracks());
    if (this._frontCamera) {
        stream.getVideoTracks()[0].stop();
        stream.removeTrack(stream.getVideoTracks()[0]);
        navigator['webkitGetUserMedia'](this._constrains["back"], (s: MediaStream) => {
          stream.addTrack(s.getVideoTracks()[0].clone());
          this.session.sendReinvite();
        }, () => {
      });

    }
    else {
      stream.getVideoTracks()[0].stop();
      stream.removeTrack(stream.getVideoTracks()[0]);
      navigator['webkitGetUserMedia'](this._constrains["face"], (s: MediaStream) => {
        stream.addTrack(s.getVideoTracks()[0].clone());
        this.session.sendReinvite();
      }, () => {
      })
    }
    this._frontCamera = !this._frontCamera;


  }

  public end() {
    if (this.session)
      this.session.terminate();
    this.type = 'terminated';
    this._inCall = false;
  }

  public stop() {
    this.phone.unregister();
    this.phone.stop();
  }

  public type;

  public call(user) {
    this.session = this.phone.invite('sip:' + user + '@sv.consultadoctor.ro', this.options);
    this._inCall = true;

    this.callbacks();

  }


  private callbacks() {
    this.session.on('accepted', () => {
      this.type = 'accepted';
      this._inCall = true;
    });

    this.session.on('failed', (e) => {
      this.type = 'failed';
      this.onEndCall(this.type);

      this._inCall = false;

    });
    this.session.on('terminated', (e) => {
      this.type = 'terminated';
      this.onEndCall(this.type);

      this._inCall = false;

    });
    this.session.on('cancel', (e) => {
      this.type = 'canceled';
      this._inCall = false;

      this.onEndCall(this.type);
    });
  }


  public soundMuted;
  public videoMuted;

  public toggleMic() {
    if (this.session) {
      if (!this.soundMuted)
        this.session.mute({audio: true, video: false});
      else
        this.session.unmute({audio: true, video: false});
      this.soundMuted = !this.soundMuted;
    }
  }

  public toggleVideo() {
    if (this.session) {
      if (!this.videoMuted)
        this.session.mute({audio: false, video: true});
      else
        this.session.unmute({audio: false, video: true});
      this.videoMuted = !this.videoMuted;
    }

  }
}
