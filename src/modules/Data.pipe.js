"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var DataPipe = (function () {
    function DataPipe() {
    }
    DataPipe.prototype.transform = function (data) {
        var t = data.split(/[- :]/);
        // Apply each element to the Date function
        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4]);
        return d;
    };
    return DataPipe;
}());
DataPipe = __decorate([
    core_1.Pipe({ name: 'data' })
], DataPipe);
exports.DataPipe = DataPipe;
