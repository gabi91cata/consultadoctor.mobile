import { Geolocation } from 'ionic-native';
export var GeoModule = (function () {
    function GeoModule() {
    }
    GeoModule.watchCoords = function (cb, timer) {
        var int = setInterval(function () {
            if (GeoModule.coords) {
                cb(GeoModule.coords);
                clearInterval(int);
            }
        }, 100);
        var c;
        setInterval(function () {
            if (c != GeoModule.coords) {
                cb(GeoModule.coords);
                c = (GeoModule.coords);
            }
        }, timer);
    };
    GeoModule.getCoords = function () {
        var latitude, longitude;
        var watch = Geolocation.watchPosition();
        var getCoordinates = function (data) {
            if (data.code != 2) {
                var lat1 = parseFloat(data.coords.latitude.toFixed(6));
                var lon1 = parseFloat(data.coords.longitude.toFixed(6));
                if (latitude != lat1 || longitude != lon1) {
                    latitude = lat1;
                    longitude = lon1;
                }
                GeoModule.coords = ({
                    lat: latitude,
                    lng: longitude,
                });
            }
        };
        Geolocation.getCurrentPosition().then(getCoordinates).catch(function () { });
        watch.subscribe(getCoordinates);
    };
    return GeoModule;
}());
//# sourceMappingURL=GeoModule.js.map