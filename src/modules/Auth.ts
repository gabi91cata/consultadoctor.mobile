
import {Global, Deferred} from "./Global";
import {User} from "../eloquent/User";
export class Auth {


  public email;
  public password;
  constructor()
  {

  }

  public static setUser(user)
  {
    let string = "";

    if(typeof user  === 'string')
      string = user;
    else
      string = JSON.stringify(user);

    window.localStorage.setItem("user", string);

    for(let w of this.__watchs)
    {
        w(JSON.parse(string));
    }
  }
  public static SetToken($token)
  {
    let user:any =  Auth.user();
    Global.gcm_token = $token;
    if(!user)
      return;
    if(user && user.gcm_token != $token)
    {
      let u = new User(user);

      u.gcm_token = $token;
      u.with({gcm:1}).save();
    }
  }

  public static getUser()
  {
    let d = new Deferred();

    let user = this.user();
    if(!user)
      return d.promise;
    new User().with({sip_password:user.sip_password}).find(user.id).later((a)=>{
      if(a != user)
      Auth.setUser(a);
      d.resolve(a);
    });

    return d.promise;
  }
  public static user()
  {
    let user = window.localStorage.getItem("user");

    try{
      return JSON.parse(user);
    } catch (e){
      return null;
    }
  }
  private static __watchs = [];
  public static watch(cb)
  {
    this.__watchs.push(cb);

    cb(this.user());


  }

  public loading = false;
  public errors = [];

  /**
   * Here we submit the data to the server to verify it
   */
  public submit() {
    this.loading = true;
    this.errors = [];
    let d = new Deferred();
    Global.$http.post(Global.url+'/api/Login', this).map(res => res.json()).subscribe(result => {
      console.log(result);
      let user = result.user;
      d.resolve(user);
    },(a)=>{
      this.errors = (a.json());
      this.loading = false;
    });
    return d.promise;
  }
}
