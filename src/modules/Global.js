"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Global = (function () {
    function Global(http, toastCtrl, modal) {
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.modal = modal;
        Global.$http = http;
        Global.toast = toastCtrl;
        Global.modal = modal;
    }
    Global.showMessage = function (string, image, delay, position, duration) {
        var _this = this;
        if (image === void 0) { image = null; }
        if (delay === void 0) { delay = 100; }
        if (position === void 0) { position = 'bottom'; }
        if (duration === void 0) { duration = 6000; }
        this._messageToast = Global.toast.create({
            message: string,
            img: image,
            duration: duration,
            position: position
        });
        this._messageTimeout = setTimeout(function () {
            _this._messageToast.present().then(function () {
            }).catch(function () {
            });
        }, delay);
    };
    Global.stopMessages = function () {
        try {
            clearTimeout(this._messageTimeout);
            this._messageToast.dismiss().catch(function () {
            });
        }
        catch (e) {
        }
    };
    Global.getDistance = function (p1, p2) {
        if (!p1 || !p2)
            return -1;
        var rad = function (x) {
            return x * Math.PI / 180;
        };
        var R = 6378137; // Earth’s mean radius in meter
        var dLat = rad(p2.lat - p1.lat);
        var dLong = rad(p2.lng - p1.lng);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
                Math.sin(dLong / 2) * Math.sin(dLong / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return Math.round((R * c / 1000) * 100) / 100;
    };
    return Global;
}());
Global.url = "https://consultadoctor.ro";
Global.$mdToast = {
    show: function (message, duration) {
        if (duration === void 0) { duration = 5000; }
        var toast = Global.toast.create({
            message: message,
            duration: duration
        });
        toast.present();
    }
};
exports.Global = Global;
var Deferred = (function () {
    function Deferred() {
        var _this = this;
        this.promise = new Promise(function (resolve, reject) {
            _this.resolve = resolve;
            _this.reject = reject;
        });
    }
    return Deferred;
}());
exports.Deferred = Deferred;
