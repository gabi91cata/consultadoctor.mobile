import {Http} from "@angular/http";
import {ToastController, ModalController, Toast, NavController} from "ionic-angular";
import {AboutUsPage} from "../pages/about-us/about-us";
import {Deeplinks} from "@ionic-native/deeplinks";

export class Linking {

   constructor(private deeplinks:Deeplinks, private nav:NavController){

     console.log(deeplinks);
     this.deeplinks.route({
       '/confirm/:id':AboutUsPage,
     }).subscribe((match) => {
       console.log('Successfully matched route', match);
       this.nav.push(AboutUsPage);

     }, (nomatch) => {
       // nomatch.$link - the full link data
       console.error('Got a deeplink that didn\'t match', nomatch);
     });

   }
}
