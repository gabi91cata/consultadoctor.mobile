import {Component, Input} from '@angular/core';

@Component({
  selector: 'rating',
  template: `<ion-icon class="star" *ngFor="let item of positive" name="star"></ion-icon><ion-icon class="star" *ngFor="let item of negative"  name="star-outline" ></ion-icon>`

})
export class Rating {

  positive: any = [];
  negative: any = [];

  @Input('stars')  stars;

  constructor() {


  }

  ngOnInit() {
    let neg = 5-this.stars;
    let poz = 5-neg;
    this.positive = new Array(poz);
    this.negative = new Array(neg);

  }

}
