var port = 110;
var host = "127.0.0.1";
var pass = "123GamaSoftIT456";

var Redis = require('ioredis');
var redis = new Redis({
  port: port,          // Redis port
  host: host,   // Redis host
  family: 4,           // 4 (IPv4) or 6 (IPv6)
  password: pass,
  db: 0
});


var fs = require('fs');

var cfg = {
  ssl: true,
  port: 8081,
  ssl_key: '/yourname.key',
  ssl_cert: '/yourname.crt'
};

var httpServ = ( cfg.ssl ) ? require('https') : require('http');

var app = null;

// dummy request processing
var processRequest = function (req, res) {

  res.writeHead(200);
  res.end("All glory to WebSockets!\n");
};

if (cfg.ssl) {

  app = httpServ.createServer({

    // providing server with  SSL key/cert
    key: fs.readFileSync(cfg.ssl_key),
    cert: fs.readFileSync(cfg.ssl_cert)

  }, processRequest).listen(cfg.port);
} else {
  app = httpServ.createServer(processRequest).listen(cfg.port);
}
function guid() {
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

function s4() {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
}

var sendStatus = function (status, token) {
  for (var i in usersToSubscribe) {
    //if (usersToSubscribe[i].indexOf(token) != -1)
    {


      var s = sockets[i];
      console.log("send:to", i);
      for (var j in s) {

        if (s[j]) {
          console.log("try ", s[j].readyState);

          if (s[j].readyState == 1)
            s[j].send(JSON.stringify({
              event: "Illuminate\\Notifications\\Events\\BroadcastNotificationCreated",
              data: {type: 'App\\Notifications\\Subscribe', user: token, status: status}
            }));
        }
      }

    }
  }
}


var getStatus = function (token, ws) {
  for (var i in usersToSubscribe) {
    //if (usersToSubscribe[i].indexOf(token) != -1)
    {


      var s = sockets[i];
      console.log("send:to", i);
      for (var j in s) {
        status = "offline";

        if (s[j])
          if (s[j].readyState == 1)
            status = "online";

        ws.send(JSON.stringify({
          event: "Illuminate\\Notifications\\Events\\BroadcastNotificationCreated",
          data: {type: 'App\\Notifications\\Subscribe', user: i, status: status}
        }));

      }

    }
  }
}

var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({server: app});
var subscribes = [];
var usersToSubscribe = {};
var sockets = {};
var globalIterator = 0;



var ClientsStream = function () {
  this.clients = [];
  this.subscribers = [];
  var item = this;
  this.addUser = function (user) {
    item.clients.push(user);
  };

  this.userSubscribeTo = function (user, to) {
    if(!item.subscribers[user.token])
      item.subscribers[user.token] = [];
    to.forEach(function (i) { item.subscribers[user.token].push(i) })
  };

  this.deleteUser = function (uid) {
    item.clients.filter(function(i){ return i.uid == uid}).forEach(function (i) { delete item.clients[i] });
  };
  this.find = function (token) {
    return item.clients.filter(function(i){ return i.token == token});
  };
};
var clients = new ClientsStream();


var User = function (token, ws) {
  this.uid = guid();
  this.token = token;
  this.websocket = ws;
  this.subscribers = [];
  var item = this;

  this.subscribe = function (token) {
    clients.userSubscribeTo(item, clients.find(token));
  };

  this.sendStatusToSubscribers = function (status) {
    item.subscribers.forEach(function (user) {
      user.websocket.send(JSON.stringify({
        event: "Illuminate\\Notifications\\Events\\BroadcastNotificationCreated",
        data: {type: 'App\\Notifications\\Subscribe', user: item.token, status: status}
      }));
    })
  };

  this.delete = function () {
    clients.deleteUser(item.uid);
    item.sendStatusToSubscribers('offline');
  };
  clients.addUser(item);

};



wss.on('connection', function (ws) {
  var localIterator = globalIterator;
  globalIterator++;
  var q = ws.upgradeReq.url.split('token=');
  var token = q[1];

  var user = new User(token, ws);


  var globalChannel = "";
  if (!sockets[token])
    sockets[token] = [ws];
  else
    sockets[token].push(ws);

  usersToSubscribe[token] = [];
  ws.on('close', function () {


    delete sockets[token][sockets[token].indexOf(ws)];
    delete usersToSubscribe[token];
    delete subscribes[globalChannel][subscribes[globalChannel].indexOf(ws)];


    sendStatus('offline', token);
  });
  sendStatus('online', token);

  ws.on('message', function (message) {
    var m = JSON.parse(message);
    if (m.subscribe) {
      globalChannel = 'private-' + m.subscribe;
      redis.subscribe('private-' + m.subscribe);
      if (subscribes['private-' + m.subscribe])
        subscribes['private-' + m.subscribe].push(ws);
      else {
        subscribes['private-' + m.subscribe] = [ws];
      }
    }
    if (m.type == 'currentLocation') {
      usersToSubscribe[token] = m.data;
      console.log("send_status", token);

      getStatus(token, ws);
    }
  });
});

redis.on('message', function (channel, message) {
  console.log('Message Recieved: ' + channel);
  var ws = subscribes[channel];
  for (var i in ws) {
    if (ws[i].readyState == 1) {
      ws[i].send(message);
    }

  }

  for (var i in subscribes[channel]) {
    if (subscribes[channel][i].readyState != 1) {
      delete subscribes[channel][i];
    }
  }
});
