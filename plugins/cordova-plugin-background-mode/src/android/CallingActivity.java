package de.appplant.cordova.plugin.background;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adobe.phonegap.push.PushPlugin;
import com.gamasoft.consultadoctor.ro.MyActivity;
import com.gamasoft.consultadoctor.ro.R;

import org.apache.cordova.CordovaActivity;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONException;
import org.json.JSONObject;

public class CallingActivity extends CordovaActivity {




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        setContentView(R.layout.calling_activity);

    }

    public void onAccept(View v)
    {

        Intent myIntent = new Intent(this, MyActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        this.startActivity(myIntent);
        Bundle json = getIntent().getExtras();
        json.putString("response", "accept");
        PushPlugin.sendExtras(json);
        this.finish();

    }

    @Override
    public void onBackPressed() {

    }

    public void onCancel(View v)
    {
        Intent myIntent = new Intent(this, MyActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        this.startActivity(myIntent);
        Bundle json = getIntent().getExtras();
        json.putString("response", "cancel");
        PushPlugin.sendExtras(json);
        this.finish();
    }
}
